package corp.ny.com.lakmdsbank.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.models.OperationType;
import corp.ny.com.lakmdsbank.preference.Env;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.activity.Main;

import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.TOP;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 26/02/18.
 */

public class TransactionSucceed extends Fragment implements View.OnClickListener {

    private TextView transactionAmount;
    private FloatingActionButton btnDone;
    private Operation operation;

    public static Fragment getInstance() {
        TransactionSucceed f = new TransactionSucceed();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        TransactionSucceed f = new TransactionSucceed();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(BOTTOM).setDuration(400));
        fragment.setExitTransition(new Slide(TOP).setDuration(400));
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transaction_succeed, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        initialize(view);
    }

    private void initialize(View view) {
        Sequent.origin((ViewGroup) view).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_DOWN).start();
        findViews(view);
        if (getArguments() != null) {
            if (getArguments().containsKey(Operation.class.getName())) {
                operation = (Operation) getArguments().get(Operation.class.getName());
                assert operation != null;
                OperationType operationType = OperationTypeTable.getInstance().find(operation.getTypeID());
                transactionAmount.setText(Amount.toMoney(operation.getAmount() * operationType.getValue()));
                if (operationType.getValue() < 0) {
                    transactionAmount.setTextColor(getContext().getColor(R.color.accent));
                }
            }
        }
        ((Main) getActivity()).removeLastScreenFromHistory();
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        transactionAmount = container.findViewById(R.id.transaction_amount);
        btnDone = container.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnDone) {
            Bundle bundle = new Bundle();
            Account account = AccountTable.getInstance().find(operation.getSavingID());
            bundle.putSerializable(Account.class.getName(), account);
            bundle.putString(Env.BundleDataName.AMOUNT.name(), String.valueOf(AccountTable.getInstance().getAccountAmount(account.getId())));
            ((Main) getActivity()).displaySelectedScreen(SavingHistory.newInstance(bundle));
        }
    }


}
