package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.OperationType;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class OperationTypeTable extends Table<OperationType> {

    public static OperationTypeTable getInstance() {
        return new OperationTypeTable();
    }

    public static String buildTable() {
        return "CREATE TABLE operation_types (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `name` VARCHAR(45) NOT NULL,\n" +
                "  `value` INT(11) NOT NULL);";
    }

    public static String insertDefaultData() {
        return "INSERT INTO `operation_types` (id,name,value) VALUES (1,'Dépot',1),\n" +
                " (2,'Retrait',-1)";
    }

    @Override
    public String getTableName() {
        return "operation_types";
    }

    @Override
    public String getIdValue(OperationType obj) {
        return String.valueOf(obj.getId());
    }

    @Override
    public ContentValues sqlQueryBuilder(OperationType obj, ContentValues query) {
        if (obj.getId() != 0) {
            query.put("id", obj.getId());
        }
        query.put("name", obj.getName());
        query.put("value", obj.getValue());
        return query;
    }

    @Override
    public OperationType cursorToModel(Cursor cursor) {
        return new OperationType(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));
    }
}
