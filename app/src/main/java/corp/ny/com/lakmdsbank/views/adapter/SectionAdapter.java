package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by yann-yvan on 11/02/18.
 */

public class SectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_VARIOUS = 0;
    private final int VIEW_TYPE_DEPOSIT = 1;
    private final int VIEW_TYPE_RETIREMENT = 2;
    private Context context;
    private List<Account> accounts = new ArrayList<>();
    private SparseArray<List<String>> accountAmount = new SparseArray<>();
    private OnItemClickListener<Account> itemSelectedListener;

    public SectionAdapter(Context context) {
        this.context = context;
    }

    public boolean addItem(Account account) {
        if (accounts.add(account)) {
            notifyItemInserted(accounts.size() - 1);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("viewType", viewType + "");
        View view = LayoutInflater.from(context).inflate(R.layout.component_section, parent, false);
        CardView cardView = (CardView) view;
        SectionHolder sectionHolder;
        switch (viewType % 3) {
            case VIEW_TYPE_VARIOUS:
                cardView.setCardBackgroundColor(context.getColor(R.color.accent));
                //sectionHolder.title.setTextColor(ColorStateList.);
                return new SectionHolder(cardView);
            case VIEW_TYPE_DEPOSIT:
                cardView.setCardBackgroundColor(context.getColor(R.color.primary_dark));
                sectionHolder = new SectionHolder(cardView);
                sectionHolder.title.setTextColor(context.getColor(R.color.white_text_transparent));
                return sectionHolder;
            case VIEW_TYPE_RETIREMENT:
                cardView.setCardBackgroundColor(context.getColor(R.color.primary_light));
                sectionHolder = new SectionHolder(cardView);
                sectionHolder.amount.setTextColor(context.getColor(R.color.primary_text_dark));
                sectionHolder.title.setTextColor(context.getColor(R.color.primary_text_dark_transparent_70));
                return sectionHolder;
        }

        return null;
    }

    public void setItemSelectedListener(OnItemClickListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SectionHolder) {
            final SectionHolder sectionHolder = (SectionHolder) holder;
            //Sequent.origin((ViewGroup) sectionHolder.itemView).delay(50).duration(100).offset(10).anim(context, Animation.FADE_IN_LEFT).start();
            sectionHolder.amount.post(new Runnable() {
                @Override
                public void run() {
                    Account account = accounts.get(sectionHolder.getAdapterPosition());
                    if (accountAmount.get(account.getId(), null) == null) {
                        ArrayList<String> list = new ArrayList<>();
                        AccountTable table = AccountTable.getInstance();
                        list.add(String.valueOf(table.getAccountAmount(account.getId())));
                        list.add(String.valueOf(table.countMembers(account.getId())));
                        accountAmount.append(account.getId(), list);
                        sectionHolder.amount.setText(Amount.toMoney(Double.parseDouble(list.get(0))));
                        sectionHolder.extra.setText(context.getResources().getQuantityString(R.plurals.member_count, Integer.parseInt(list.get(1)), Integer.parseInt(list.get(1))));
                    } else {
                        sectionHolder.amount.setText(Amount.toMoney(Double.parseDouble(accountAmount.get(account.getId()).get(0))));
                        int memberCount = Integer.parseInt(accountAmount.get(account.getId()).get(1));
                        sectionHolder.extra.setText(context.getResources().getQuantityString(R.plurals.member_count, memberCount, memberCount));
                    }
                    sectionHolder.title.setText(account.getName());
                }
            });
            sectionHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null) {
                        itemSelectedListener.click(accounts.get(sectionHolder.getAdapterPosition()));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    public String getAccountAmount(Account account) {
        if (accountAmount.get(account.getId(), null) != null) {
            return String.valueOf(accountAmount.get(account.getId()).get(0));
        } else {
            return Amount.toMoney(0f);
        }
    }

    private class SectionHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView amount;
        private TextView extra;

        public SectionHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        /**
         * Find the Views in the layout<br />
         */
        private void findViews(View rootView) {
            title = rootView.findViewById(R.id.title);
            amount = rootView.findViewById(R.id.amount);
            extra = rootView.findViewById(R.id.extra);
        }
    }

}
