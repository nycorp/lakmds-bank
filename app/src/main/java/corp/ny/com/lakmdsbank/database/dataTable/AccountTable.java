package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Operation;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class AccountTable extends Table<Account> {

    public static String buildTable() {
        return "CREATE TABLE IF NOT EXISTS " + AccountFields.accounts.name() + " (\n" +
                "  " + AccountFields.id.name() + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  " + AccountFields.type_id.name() + " INT NOT NULL,\n" +
                "  " + AccountFields.name.name() + " VARCHAR(45) NOT NULL,\n" +
                "  CONSTRAINT `fk_accounts_types1`\n" +
                "    FOREIGN KEY (" + AccountFields.type_id.name() + ")\n" +
                "    REFERENCES " + AccountTypeTable.AccountTypeFields.account_types.name() +
                "    (" + AccountTypeTable.AccountTypeFields.id.name() + ")\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE);\n";
    }

    public static String insertDefaultData() {
        return "INSERT INTO " + AccountFields.accounts.name() +
                "(" + AccountFields.id.name() + "," + AccountFields.name.name() + ","
                + AccountFields.type_id.name() +
                ") VALUES (1,'Divers',1)," +
                "(2,'Mon compte personnel',1)," +
                "(3,'Mon compte famille',1)," +
                "(4,'Mon compte Amie',1);";
    }

    public static AccountTable getInstance() {
        return new AccountTable();
    }

    @Override
    public int getLimit() {
        return 12;
    }

    @Override
    public String getTableName() {
        return AccountFields.accounts.name();
    }

    @Override
    public String getIdValue(Account obj) {
        return String.valueOf(obj.getId());
    }

    public String getAccountName() {
        return AccountFields.name.name();
    }

    /**
     * Count member's account in participation table
     *
     * @param id of the member
     * @return the total of account for a member
     */
    public int countMembers(int id) {
        int total = 0;
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM participations WHERE savings_id = %s", id), null);
        if (cursor != null) {
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }

    /**
     * Method for find information get result list
     *
     * @return T
     */
    public ArrayList<Account> findAll(int id) {
        ArrayList<Account> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE NOT IN SELECT" +
                "savings_id FROM participations WHERE members_id NOT LIKE %s ORDER BY %s", getTableName(), id, getOrderBy()), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    @Override
    public String getOrderBy() {
        return AccountFields.name.name();
    }

    public double getBankAmount() {
        OperationTable table = new OperationTable();
        double amount = 0f;
        for (Operation op : table.findAll()) {
            amount += op.getAmount() * table.getOperationType(op.getTypeID()).getValue();
        }
        return amount;
    }

    /**
     * Method to find model by <i>name</i>
     *
     * @param name represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public boolean checkIfExist(String name) {
        Cursor cursor = getDb().query(getTableName(), null, "name LIKE ?",
                new String[]{name}, null, null, null);
        if (cursor != null) {
            return cursor.moveToNext();
        }
        Log.e("found", "false");
        return false;
    }

    /**
     * @param id
     * @return
     */
    public double getAccountAmount(int id) {
        OperationTable table = new OperationTable();
        double amount = 0f;
        for (Operation op : table.getAccountTransaction(id)) {
            amount += op.getAmount() * table.getOperationType(op.getTypeID()).getValue();
        }
        return amount;
    }

    @Override
    public ContentValues sqlQueryBuilder(Account obj, ContentValues query) {
        if (obj.getId() != 0) {
            query.put(AccountFields.id.name(), obj.getId());
        }
        query.put(AccountFields.type_id.name(), obj.getTypeID());
        query.put(AccountFields.name.name(), toFCUpperCase(obj.getName()));
        return query;
    }

    @Override
    public Account cursorToModel(Cursor cursor) {
        return new Account(cursor.getInt(0), cursor.getInt(1), cursor.getString(2));
    }

    private enum AccountFields {
        accounts,
        id,
        type_id,
        name
    }
}
