package corp.ny.com.lakmdsbank.views.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.util.ArrayList;
import java.util.Calendar;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTypeTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.models.OperationType;
import corp.ny.com.lakmdsbank.preference.Env;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.utils.DateManger;
import corp.ny.com.lakmdsbank.utils.EntryManager;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.MemberSpinnerAdapter;
import corp.ny.com.lakmdsbank.views.adapter.OperationTypeSpinnerAdapter;
import corp.ny.com.lakmdsbank.views.adapter.SectionAdapter;
import corp.ny.com.lakmdsbank.views.adapter.SpinnerAdapter;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class Transaction extends Fragment implements View.OnClickListener, OnItemClickListener {

    private ImageView btnBack;
    private EditText transactionAmount;
    private TextView transactionAmountError;
    private LinearLayout accountSlot;
    private ImageView transactionImage;
    private TextView savingName;
    private TextView accountType;
    private TextView bankAmount;
    private ImageView btnChooseAccount;
    private Spinner autoTvTransactionSender;
    private EditText tvTransactionDesc;
    private EditText transactionDate;
    private Spinner spTransactionType;
    private CircularProgressButton btnMakeTransaction;
    private RecyclerView sectionRecycleView;
    private TextView memberNameError;
    private TextView transactionDescError;
    private TextView transactionTypeError;
    //model
    private Operation operation;
    private Account account;
    private Member member;

    public static Fragment getInstance() {
        Transaction f = new Transaction();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        Transaction f = new Transaction();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(100));
        fragment.setExitTransition(new Slide(START).setDuration(100));
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_make_transaction, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        Sequent.origin((ViewGroup) view).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        findViews(view);
        operation = Operation.getInstance();

        if (getArguments() != null) {
            if (getArguments().containsKey(getClass().getName())) {
                account = (Account) getArguments().get(getClass().getName());
                assert account != null;
                savingName.setText(account.getName());
                accountType.post(new Runnable() {
                    @Override
                    public void run() {
                        accountType.setText(AccountTypeTable.getInstance().find(account.getTypeID()).getName());
                    }
                });
                operation.setSavingID(account.getId());
            }

            if (getArguments().containsKey(Env.BundleDataName.AMOUNT.name())) {
                bankAmount.setText(Amount.toMoney(getArguments().getString(Env.BundleDataName.AMOUNT.name())));
            }
        }

        sectionRecycleView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        final SectionAdapter sectionAdapter = new SectionAdapter(getContext());
        sectionAdapter.setItemSelectedListener(this);
        sectionRecycleView.setAdapter(sectionAdapter);
        sectionAdapter.setItemSelectedListener(this);
        sectionRecycleView.post(new Runnable() {
            @Override
            public void run() {
                for (Account ac : AccountTable.getInstance().findAll()) {
                    sectionAdapter.addItem(ac);
                }
            }
        });

        spTransactionType.post(new Runnable() {
            @Override
            public void run() {
                SpinnerAdapter adapter = new OperationTypeSpinnerAdapter(getContext(),
                        R.layout.component_spinner_view, new OperationTypeTable().findAll());
                spTransactionType.setAdapter(adapter);
                adapter.setItemClickListener(Transaction.this);
            }
        });

        updateTransactionSenderList();
        btnChooseAccount.setRotation(180);

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        setTransactionDate(mYear, mMonth, mDay);
    }

    private void updateTransactionSenderList() {
        autoTvTransactionSender.post(new Runnable() {
            @Override
            public void run() {
                SpinnerAdapter adapter;
                if (account != null) {
                    adapter = new MemberSpinnerAdapter(getContext(),
                            R.layout.component_spinner_view, MemberTable.getInstance().findAllFromAccount(account.getId()));
                } else {
                    adapter = new MemberSpinnerAdapter(getContext(),
                            R.layout.component_spinner_view, new ArrayList<Member>());
                }
                adapter.setItemClickListener(Transaction.this);
                autoTvTransactionSender.setAdapter(adapter);
            }
        });

    }

    public boolean getError() {
        if (TextUtils.isEmpty(transactionAmount.getText())) {
            transactionAmountError.setText("Saisir un montant");
            EntryManager.displayError(transactionAmount, transactionAmountError);
            return true;
        } else if (Double.valueOf(transactionAmount.getText().toString()) < 25) {
            transactionAmountError.setText("Saisir un montant supérieur a XAF25");
            EntryManager.displayError(transactionAmount, transactionAmountError);
            return true;
        } else if (member == null) {
            memberNameError.setText("Choisir un l'adherent");
            EntryManager.displayError(autoTvTransactionSender, memberNameError);
            return true;
        } else if (operation == null) {
            transactionTypeError.setText("Choisir le type de transaction");
            EntryManager.displayError(spTransactionType, transactionTypeError);
            return true;
        }

        return false;
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        Sequent.origin((ViewGroup) container).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        btnBack = container.findViewById(R.id.btn_back);
        transactionAmount = container.findViewById(R.id.transaction_amount);
        transactionAmountError = container.findViewById(R.id.transaction_amount_error);
        accountSlot = container.findViewById(R.id.account_slot);
        transactionImage = container.findViewById(R.id.transaction_image);
        savingName = container.findViewById(R.id.saving_name);
        accountType = container.findViewById(R.id.account_type);
        bankAmount = container.findViewById(R.id.bank_amount);
        btnChooseAccount = container.findViewById(R.id.btn_choose_account);
        autoTvTransactionSender = container.findViewById(R.id.auto_tv_transaction_sender);
        tvTransactionDesc = container.findViewById(R.id.tv_transaction_desc);
        spTransactionType = container.findViewById(R.id.sp_transaction_type);
        btnMakeTransaction = container.findViewById(R.id.btn_make_transaction);
        sectionRecycleView = container.findViewById(R.id.section_recycle_view);
        transactionDescError = container.findViewById(R.id.transaction_desc_error);
        transactionTypeError = container.findViewById(R.id.transaction_type_error);
        memberNameError = container.findViewById(R.id.member_name_error);
        transactionDate = container.findViewById(R.id.transaction_date);

        btnMakeTransaction.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnChooseAccount.setOnClickListener(this);
        tvTransactionDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateSubmitButton();
                EntryManager.hideError(transactionAmount, transactionAmountError);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        transactionAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateSubmitButton();
                EntryManager.hideError(tvTransactionDesc, transactionDescError);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        transactionDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) selectDate();
            }
        });
        transactionDate.setShowSoftInputOnFocus(false);
        transactionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate();
            }
        });
    }


    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            getActivity().onBackPressed();
        } else if (v == btnMakeTransaction) {
            btnMakeTransaction.startAnimation();
            btnMakeTransaction.setDoneColor(R.color.success);
            operation.setDate(transactionDate.getText().toString());
            operation.setDescription(tvTransactionDesc.getText().toString());
            operation.setAmount(Double.parseDouble(transactionAmount.getText().toString()));
            if (!getError()) {
                //
                btnMakeTransaction.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnMakeTransaction.stopAnimation();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Operation.class.getName(), operation);
                        ((Main) getActivity()).addToHistory(LockScreen.newInstance(bundle));
                    }
                }, 1500);
            } else {
                btnMakeTransaction.revertAnimation();
            }
        } else if (v == btnChooseAccount) {
            switchAccount();
        }
    }

    public void switchAccount() {
        if (sectionRecycleView.getVisibility() == View.GONE) {
            accountSlot.setVisibility(View.GONE);
            sectionRecycleView.setVisibility(View.VISIBLE);
            Sequent.origin(sectionRecycleView).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_UP).start();
        } else {
            sectionRecycleView.setVisibility(View.GONE);
            accountSlot.setVisibility(View.VISIBLE);
            Sequent.origin(accountSlot).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_DOWN).start();
            btnChooseAccount.setRotation(180);
        }

    }

    /**
     * Enable submit button
     */
    public void activateSubmitButton() {
        if (operation.getSavingID() != 0
                && operation.getMemberID() != 0
                && transactionAmount.getText().length() > 0
                && operation.getTypeID() > 0) {
            btnMakeTransaction.setEnabled(true);
            btnMakeTransaction.setBackground(getContext().getDrawable(R.drawable.active_dot));
            return;
        }
        btnMakeTransaction.revertAnimation();
        btnMakeTransaction.setEnabled(false);
        btnMakeTransaction.setBackground(getContext().getDrawable(R.drawable.inactive_dot));
    }

    public void selectDate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Time Picker Dialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        setTransactionDate(year, monthOfYear, dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void setTransactionDate(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        transactionDate.setText(String.format("%s-%s-%s %s:%s:%s",
                year, DateManger.dateZeroFormatter(month + 1), DateManger.dateZeroFormatter(day), DateManger.dateZeroFormatter(c.get(Calendar.HOUR_OF_DAY)),
                DateManger.dateZeroFormatter(c.get(Calendar.MINUTE)), DateManger.dateZeroFormatter(c.get(Calendar.SECOND))));

    }

    @Override
    public void click(final Object model) {
        if (model instanceof OperationType) {
            if (((OperationType) model).getId() == 0) {
                EntryManager.displayError(spTransactionType, transactionTypeError);
                operation.setTypeID(0);
            } else {
                EntryManager.hideError(spTransactionType, transactionTypeError);
                operation.setTypeID(((OperationType) model).getId());
            }
        } else if (model instanceof Account) {
            account = (Account) model;
            switchAccount();
            savingName.setText(account.getName());
            bankAmount.post(new Runnable() {
                @Override
                public void run() {
                    bankAmount.setText(Amount.toMoney(AccountTable.getInstance().getAccountAmount(account.getId())));
                }
            });
            accountType.post(new Runnable() {
                @Override
                public void run() {
                    accountType.setText(AccountTypeTable.getInstance().find(account.getTypeID()).getName());
                }
            });
            operation.setSavingID(account.getId());

            updateTransactionSenderList();
        } else if (model instanceof Member) {
            member = (Member) model;
            if (member.getId() == 0) {
                EntryManager.displayError(autoTvTransactionSender, memberNameError);
            } else {
                EntryManager.hideError(autoTvTransactionSender, memberNameError);
                operation.setMemberID(member.getId());
            }
        }
        activateSubmitButton();
    }

}
