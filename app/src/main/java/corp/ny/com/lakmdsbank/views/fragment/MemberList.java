package corp.ny.com.lakmdsbank.views.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.MemberListAdapter;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

import static android.app.Activity.RESULT_OK;
import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class MemberList extends Fragment implements View.OnClickListener, OnItemClickListener<Member> {
    private static final int RESULT_PICK_CONTACT = 1995;
    private ImageView btnBack;
    private TextView tvAmount;
    private RecyclerView memberList;
    private FloatingActionButton buttonActionCreate;
    private FloatingActionButton buttonActionImport;
    private FloatingActionMenu floatingActionMenu;
    private Account account;
    private String money;

    public static Fragment getInstance() {
        MemberList f = new MemberList();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        MemberList f = new MemberList();
        defaultAnimation(f);
        f.setArguments(bundle);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(400));
        fragment.setExitTransition(new Slide(START).setDuration(400));
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_member_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
        memberList.setLayoutManager(new LinearLayoutManager(getContext()));
        MemberListAdapter adapter = new MemberListAdapter(getContext());
        adapter.addItem(MemberTable.getInstance().findAll());
        adapter.setItemSelectedListener(this);
        memberList.setAdapter(adapter);
        floatingActionMenu.setClosedOnTouchOutside(true);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == buttonActionCreate) {
            // Handle clicks for fbAction
            MemberDetail.getInstance().show(getFragmentManager(), "Create account");
        } else if (v == btnBack) {
            getActivity().onBackPressed();
        } else if (v == buttonActionImport) {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
        }

    }

    @Override
    public void click(Member model) {
        //prepare bundle to share data
        Bundle bundle = new Bundle();
        bundle.putSerializable(Member.class.getName(), model);
        //add screen to history
        ((Main) getActivity()).addToHistory(MemberInfo.newInstance(bundle));
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        btnBack = container.findViewById(R.id.btn_back);
        tvAmount = container.findViewById(R.id.tv_amount);
        memberList = container.findViewById(R.id.member_list);
        buttonActionCreate = container.findViewById(R.id.button_action_create);
        buttonActionImport = container.findViewById(R.id.button_action_import);
        floatingActionMenu = container.findViewById(R.id.fab_menu);

        buttonActionCreate.setOnClickListener(this);
        buttonActionImport.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     *
     * @param data
     */
    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            Member member = Member.getInstance();
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor == null) return;
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            member.setPhone(cursor.getString(phoneIndex));
            member.setName(cursor.getString(nameIndex));
            cursor.close();
            // Set the value to the textviews
            Bundle bundle = new Bundle();
            bundle.putSerializable(Member.class.getName(), member);
            //((Main) getActivity()).displaySelectedScreen(MemberDetail.getInstance());
            MemberDetail.newInstance(bundle, MemberDetail.DEFAULT_CONTEXT).show(getFragmentManager(), "Create member");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
