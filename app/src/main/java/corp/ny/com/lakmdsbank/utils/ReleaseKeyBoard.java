package corp.ny.com.lakmdsbank.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yann-yvan on 23/11/17.
 */

public class ReleaseKeyBoard {
    private InputMethodManager imm;
    private List<View> input;
    private Context context;

    public ReleaseKeyBoard(Context context) {
        this.context = context;
        imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public boolean hide(View view) {
        return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void addView(View view) {
        if (input == null) {
            input = new ArrayList<>();
        }
        input.add(view);
    }

    public void apply() {
        for (View view : input) {
            hide(view);
        }
    }
}
