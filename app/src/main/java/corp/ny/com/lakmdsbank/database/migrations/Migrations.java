package corp.ny.com.lakmdsbank.database.migrations;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTypeTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.database.dataTable.ParticipationTable;

/**
 * Created by yann-yvan on 04/12/17.
 */

public class Migrations extends SQLiteOpenHelper {

    public Migrations(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MemberTable.buildTable());//member Table creation
        db.execSQL(AccountTypeTable.buildTable());//account type Table creation
        db.execSQL(AccountTypeTable.insertDefaultData());//fill account type Table with default data
        db.execSQL(AccountTable.buildTable());//account table creation
        db.execSQL(AccountTable.insertDefaultData());//fill account Table with default data
        db.execSQL(ParticipationTable.buildTable());//participation type Table creation
        db.execSQL(OperationTypeTable.buildTable());//operation type Table creation
        db.execSQL(OperationTypeTable.insertDefaultData());//fill operation type Table with default data
        db.execSQL(OperationTable.buildTable());//operation table creation
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
        super.onConfigure(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void onLogout(SQLiteDatabase db) {
        onCreate(db);
    }

}
