package corp.ny.com.lakmdsbank.views.listener;

/**
 * Created by yann-yvan on 17/02/18.
 */

public interface OnItemClickListener<T> {
   public void click(T model);
}
