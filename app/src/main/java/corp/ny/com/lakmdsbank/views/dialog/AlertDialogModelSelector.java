package corp.ny.com.lakmdsbank.views.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.models.Pack;
import corp.ny.com.lakmdsbank.utils.ClassBuilder;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 10/04/18.
 */

public class AlertDialogModelSelector<T> implements
        AlertDialog.OnDismissListener, AlertDialog.OnMultiChoiceClickListener {
    private final String ID = "id";
    private AlertDialog.Builder dialog;
    private List<T> models = new ArrayList<>();
    private List<T> selectedModels = new ArrayList<>();
    private String labelColumn;
    private Pack<CharSequence, Boolean> multiChoiceItems;
    private DialogInterface.OnClickListener onClickListener;
    private Context context;

    public AlertDialogModelSelector(Context context, List<T> models, String labelColumn) {
        this.context = context;
        this.models = models;
        this.labelColumn = labelColumn;
    }

    /**
     * Get selected models string
     *
     * @return string representing selected data
     */
    public String getSelectedItemsString() {
        try {
            return formatSelectedItems();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get selected models
     *
     * @return list of models
     */
    public List<T> getSelectedModels() {
        return selectedModels;
    }

    public void setSelectedModels(List<T> selectedModels) {
        this.selectedModels.addAll(selectedModels);
        try {
            getDialog();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a dialog builder with data already displayed
     *
     * @return alert dialog builder with data
     * @throws ClassNotFoundException when class not found
     */
    public AlertDialog.Builder getDialog() throws ClassNotFoundException {
        dialog = new AlertDialog.Builder(context);
        dialog.setIcon(R.drawable.ic_safe);
        dialog.setTitle(R.string.prompt_choose_account);
        buildMultiChoiceItems();
        CharSequence[] sequences = new CharSequence[multiChoiceItems.keySet().toArray().length];
        boolean[] checked = new boolean[multiChoiceItems.keySet().toArray().length];
        for (int i = 0; i < multiChoiceItems.keySet().toArray().length; i++) {
            checked[i] = (boolean) multiChoiceItems.values().toArray()[i];
        }

        dialog.setMultiChoiceItems(multiChoiceItems.keySet().toArray(sequences), checked, this);

        dialog.setNegativeButton(R.string.action_back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.setPositiveButton(R.string.action_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onClickListener != null)
                    onClickListener.onClick(dialog, which);
            }
        });
        return dialog;
    }

    /**
     * Define what to do when saving your choose
     *
     * @param onClickListener the listener to fire
     */
    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * Build a couple af value visible label and
     *
     * @throws ClassNotFoundException when class not found
     */
    private void buildMultiChoiceItems() throws ClassNotFoundException {
        if (multiChoiceItems != null) return;
        multiChoiceItems = new Pack<>();
        for (T model : models) {
            //Log.e("model", model.toString());
            //Log.e("isSelect", String.valueOf(isSelected(model)));
            multiChoiceItems.put(ClassBuilder.getString(model, labelColumn),
                    isSelected(model));
        }
    }

    /**
     * Check if a model is selected
     *
     * @param model to find in selected model list
     * @return true if model found and false if not
     * @throws ClassNotFoundException when class not found
     */
    private boolean isSelected(T model) throws ClassNotFoundException {
        for (T selectedModel : selectedModels) {
            if (ClassBuilder.getInt(selectedModel, ID) == ClassBuilder.getInt(model, ID))
                return true;
        }
        return false;
    }

    /**
     * Update selected models
     *
     * @param which     model position
     * @param isChecked status
     * @throws ClassNotFoundException when class not found
     */
    private void updateSelectedItems(int which, boolean isChecked) throws ClassNotFoundException {
        multiChoiceItems.updateValue(
                ClassBuilder.getString(models.get(which), labelColumn),
                isChecked
        );
        if (isChecked) {
            selectedModels.add(models.get(which));
        } else {
            for (T model : selectedModels) {
                if (isSelected(models.get(which))) {
                    selectedModels.remove(model);
                    return;
                }
            }
        }
    }

    /**
     * Generate selected model label column
     *
     * @return a string of selected element
     * @throws ClassNotFoundException when class not found
     */
    private String formatSelectedItems() throws ClassNotFoundException {
        String format = "";
        for (T model : selectedModels) {
            if (format.isEmpty()) format += ClassBuilder.getString(model, labelColumn);
            else
                format = String.format("%s\n%s", format,
                        ClassBuilder.getString(model, labelColumn));
        }
        return format;
    }


    /**
     * Display alert dialog
     *
     * @throws ClassNotFoundException when class not found
     */
    public void show() throws ClassNotFoundException {
        getDialog();
        dialog.create().show();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        selectedModels.clear();
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        try {
            updateSelectedItems(which, isChecked);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
