package corp.ny.com.lakmdsbank.views.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.system.BuildConfig;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 17/04/18.
 */

public class About extends Fragment {
    public static About getInstance() {
        return new About();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Element versionElement = new Element();
        versionElement.setTitle(String.format("Version : %s", BuildConfig.VERSION_NAME));

        final Element author = new Element();
        author.setTitle(String.format("%s : %s", getString(R.string.contact), getString(R.string.app_author_contact)));
        author.setValue(getString(R.string.app_author_contact));
        author.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + author.getValue()));
                if (call.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(call);
                }
            }
        });

        return new AboutPage(getContext())
                .isRTL(false)
                .setImage(R.drawable.logo)
                .setDescription(getString(R.string.app_description))
                .addItem(versionElement)
                .addItem(author)
                .addGroup("Connect with us")
                .addEmail(getString(R.string.app_author_email), getString(R.string.app_author_email))
                .create();
    }
}
