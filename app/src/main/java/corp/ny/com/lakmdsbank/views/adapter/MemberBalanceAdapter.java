package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class MemberBalanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_DEFAULT = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private OnItemClickListener<Account> itemSelectedListener;
    private Context context;
    private List<Account> accounts = new ArrayList<>();
    private Member member;

    public MemberBalanceAdapter(Context context, Member member) {
        this.context = context;
        this.member = member;
    }

    @Override
    public int getItemViewType(int position) {
        return (accounts.isEmpty() ? VIEW_TYPE_DEFAULT : VIEW_TYPE_ITEM);
    }

    public boolean addItem(List<Account> account) {
        accounts.addAll(account);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_DEFAULT:
                return NoItemFoundViewHolder.build(context, parent);

            case VIEW_TYPE_ITEM:
                return new AccountHolder(LayoutInflater.from(context).inflate(R.layout.component_account_balance, parent, false));

            default:
                return NoItemFoundViewHolder.build(context, parent);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof AccountHolder) {
            final AccountHolder sectionHolder = (AccountHolder) holder;
            final Account account = accounts.get(position);
            Sequent.origin((ViewGroup) sectionHolder.itemView).delay(50).duration(100).offset(10).anim(context, Animation.FADE_IN_LEFT).start();
            sectionHolder.accountName.setText(account.getName());
            if ((position + 1) % 2 == 0) {
                sectionHolder.itemView.setBackgroundColor(context.getColor(R.color.primary));
            } else {
                sectionHolder.itemView.setBackgroundColor(context.getColor(R.color.primary_light));
            }
            double amount = MemberTable.getInstance().getSavingBalance(member.getId(), account.getId());
            if (amount < 0) {
                sectionHolder.totalAmount.setText(Amount.toMoney(amount));
                sectionHolder.totalAmount.setTextColor(context.getColor(R.color.accent));
            } else {
                sectionHolder.totalAmount.setText(Amount.toMoney(amount));
            }
            sectionHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(account);
                }
            });
        } else if (holder instanceof NoItemFoundViewHolder) {
            NoItemFoundViewHolder noItemFoundViewHolder = (NoItemFoundViewHolder) holder;
        }

    }

    @Override
    public int getItemCount() {
        return (accounts.isEmpty() ? 1 : accounts.size());
    }

    public void setItemSelectedListener(OnItemClickListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    private class AccountHolder extends RecyclerView.ViewHolder {
        private TextView accountName;
        private TextView totalAmount;

        public AccountHolder(final View itemView) {
            super(itemView);
            findViews(itemView);
        }

        /**
         * Find the Views in the layout<br />
         */
        private void findViews(View container) {
            accountName = container.findViewById(R.id.account_name);
            totalAmount = container.findViewById(R.id.account_balance);
        }

    }
}
