package corp.ny.com.lakmdsbank.models;

import java.io.Serializable;

/**
 * Created by yann-yvan on 20/02/18.
 */

public class Operation implements Serializable {
    private int id;
    private int memberID;
    private int savingID;
    private int typeID;
    private double amount;
    private String description;
    private String date;

    private Operation() {
    }

    public Operation(int id, int memberID, int savingID, int typeID, double amount, String description, String date) {
        this.id = id;
        this.memberID = memberID;
        this.savingID = savingID;
        this.typeID = typeID;
        this.amount = amount;
        this.description = description;
        this.date = date;
    }

    public static Operation getInstance() {
        return new Operation();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public int getSavingID() {
        return savingID;
    }

    public void setSavingID(int savingID) {
        this.savingID = savingID;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", memberID=" + memberID +
                ", savingID=" + savingID +
                ", typeID=" + typeID +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
