package corp.ny.com.lakmdsbank.models;

/**
 * Created by yann-yvan on 20/02/18.
 */

public class AccountType {
    private int id;
    private String name;

    public AccountType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private AccountType() {
    }

    public static AccountType getInstance() {
        return new AccountType();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AccountType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
