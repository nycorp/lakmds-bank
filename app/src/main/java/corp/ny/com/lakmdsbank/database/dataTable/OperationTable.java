package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.models.OperationType;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class OperationTable extends Table<Operation> {

    public static String buildTable() {
        return "CREATE TABLE IF NOT EXISTS operations (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `members_id` INT NOT NULL,\n" +
                "  `savings_id` INT NOT NULL,\n" +
                "  `type_operations_id` INT NOT NULL,\n" +
                "  `amount` DOUBLE NOT NULL,\n" +
                "  `description` VARCHAR(120) NULL,\n" +
                "  `date` TIMESTAMP(12) NOT NULL,\n" +
                "  CONSTRAINT `fk_members_has_savings1_members1`\n" +
                "    FOREIGN KEY (`members_id`)\n" +
                "    REFERENCES `members` (`id`)\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE,\n" +
                "  CONSTRAINT `fk_members_has_savings1_savings1`\n" +
                "    FOREIGN KEY (`savings_id`)\n" +
                "    REFERENCES `accounts` (`id`)\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE,\n" +
                "  CONSTRAINT `fk_operations_type_operations1`\n" +
                "    FOREIGN KEY (`type_operations_id`)\n" +
                "    REFERENCES `operation_types` (`id`)\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE);";
    }

    public static OperationTable getInstance() {
        return new OperationTable();
    }

    @Override
    public String getOrderBy() {
        return "date DESC";
    }

    @Override
    public int getLimit() {
        return 10;
    }

    @Override
    public String getTableName() {
        return OperationFields.operations.name();
    }

    OperationType getOperationType(int id) {
        return new OperationTypeTable().find(id);
    }

    @Override
    public String getIdValue(Operation obj) {
        return String.valueOf(obj.getId());
    }

    /**
     * Find account's transactions
     *
     * @param id of the target account
     * @return a list of operations found or an<b>empty list</b> if nothing found in table
     */
    public List<Operation> getAccountTransaction(int id) {
        ArrayList<Operation> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s",
                getTableName(), id, OperationFields.savings_id.name(), getOrderBy()), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Find member's transactions in any account
     *
     * @param id of the target member
     * @return a list of operations found or an<b>empty list</b> if nothing found in table
     */
    List<Operation> getMemberTransaction(int id) {
        ArrayList<Operation> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = %s ORDER BY %s",
                getTableName(), OperationFields.members_id.name(), id, getOrderBy()), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }


    public boolean delete(int memberId, int accountId) {
        int success = getDb().delete(getTableName(), String.format("%s = ? AND %s = ?", OperationFields.members_id, OperationFields.savings_id), new String[]{String.valueOf(memberId), String.valueOf(accountId)});
        return success > 0;
    }

    /**
     * @param memberId
     * @param accountId
     * @return
     */
    public List<Operation> getMemberTransaction(int memberId, int accountId) {
        ArrayList<Operation> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = %s AND %s = %s ORDER BY %s",
                getTableName(), OperationFields.members_id.name(), memberId, OperationFields.savings_id, accountId, getOrderBy()), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }


    @Override
    public ContentValues sqlQueryBuilder(Operation obj, ContentValues query) {
        if (obj.getId() != 0) {
            query.put("id", obj.getId());
        }
        query.put("members_id", obj.getMemberID());
        query.put("savings_id", obj.getSavingID());
        query.put("type_operations_id", obj.getTypeID());
        query.put("amount", obj.getAmount());
        query.put("description", obj.getDescription());
        query.put("date", obj.getDate());
        return query;
    }

    @Override
    public Operation cursorToModel(Cursor cursor) {
        return new Operation(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
                cursor.getInt(3), cursor.getDouble(4), cursor.getString(5), cursor.getString(6));
    }


    private enum OperationFields {
        operations,
        id,
        members_id,
        savings_id,
        type_operations_id,
        amount,
        description,
        date
    }
}
