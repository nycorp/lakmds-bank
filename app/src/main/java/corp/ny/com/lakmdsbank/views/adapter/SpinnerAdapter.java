package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 24/02/18.
 */

public abstract class SpinnerAdapter<T> extends ArrayAdapter<T> {
    private final LayoutInflater inflater;
    private final int resource;
    private List<T> itemList = new ArrayList<>();
    private OnItemClickListener<T> itemClickListener;

    public SpinnerAdapter(@NonNull Context context, @LayoutRes int resource, List<T> itemList) {
        super(context, resource, itemList);
        this.inflater = LayoutInflater.from(context);
        this.resource = resource;
        this.itemList = itemList;
    }

    public List<T> getItemList() {
        return itemList;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public int getResource() {
        return resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = createItemView(position, convertView, parent);
        view.setPadding(0, 0, 0, 0);
        if (itemClickListener != null) {
            itemClickListener.click(itemList.get(position));
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = createItemView(position, convertView, parent);
        view.setPadding(8, 0, 0, 8);
        return view;
    }

    public void setItemClickListener(OnItemClickListener<T> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    abstract View createItemView(int position, View convertView, ViewGroup parent);
}
