package corp.ny.com.lakmdsbank.database.migrations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import corp.ny.com.lakmdsbank.system.App;

/**
 * Created by yann-yvan on 05/12/17.
 * Current DATABASE  VERSION = 1
 */

public abstract class Table<T> {

    // the name of the file of our database
    public final static String NAME = "lakmdsbank.db";

    // Increment this value if you to update database
    public final static int VERSION = 1;
    private SQLiteDatabase mDb = null;


    //getTable() identify
    private String idName = "id";
    //limit
    private int limit = 5;
    //handled model
    private T model;
    //in case of need of cursor value
    private Cursor cloneCursor;

    public Table() {
        mDb = App.getDataBaseInstance();
    }

    /**
     * Current database version
     *
     * @return an integer that represent <b>database version</b>
     */
    public static int getVERSION() {
        return VERSION;
    }

    public T getModel() {
        return model;
    }

    /**
     * Get database instance
     *
     * @return {MySqLiteDatabase} instance
     */
    protected SQLiteDatabase getDb() {
        return mDb;
    }

    /**
     * Define table name
     *
     * @return table name
     */
    public abstract String getTableName();

    /**
     * Define by which column result should be order
     *
     * @return column name
     */
    public String getOrderBy() {
        return idName;
    }

    /**
     * Get the value of the used model
     *
     * @param obj the target model from which id should be taken
     * @return id value
     */
    public abstract String getIdValue(T obj);

    /**
     * Define table identifier column
     *
     * @return identify column name
     * Default identify column name is <b>id</b>
     */
    public String getIdName() {
        return idName;
    }

    /**
     * Define table limit result per query by range
     *
     * @return the number of row to return per query
     * Default value is <b>5</b>
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Insert values into a table
     *
     * @param obj the model to save in database
     * @return the model inserted into the table on <b>null</b> if something went wrong
     */
    public T create(T obj) {
        long success = getDb().insert(getTableName(), null, sqlQueryBuilder(obj, new ContentValues()));
        if (success > 0) {
            Log.e("new Id ", String.valueOf(success));
            return find(success);
        }
        return null;
    }

    /**
     * Get last select query cursor
     * @return a cursor
     */
    public Cursor getCloneCursor() {
        return cloneCursor;
    }

    /**
     * Method for delete
     *
     * @param obj the object to delete
     * @return boolean true if success
     */
    public boolean delete(T obj) {
        int success = getDb().delete(getTableName(), getIdName() + "=?", new String[]{getIdValue(obj)});
        return success > 0;
    }

    /**
     * Count the total of row in the table
     *
     * @return total of row
     */
    public int count() {
        int total = 0;
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s", getTableName()), null);
        if (cursor != null) {
            cloneCursor = cursor;
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }


    /**
     * Update a model in the table
     *
     * @param obj the model to update in database
     * @return the model up to date from the table on <b>null</b> if something went wrong
     */
    public T update(T obj) {
        int success = getDb().update(getTableName(), sqlQueryBuilder(obj, new ContentValues()), getIdName() + "=?", new String[]{getIdValue(obj)});
        if (success > 0) {
            Log.e("new Id ", String.valueOf(success));
            return find(success);
        }
        return null;
    }

    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(String id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{id}, null, null, null);
        if (cursor != null) {
            cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(int id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(long id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find all model from a table
     *
     * @return a list of model found or an<b>empty list</b> if nothing found in table
     */
    public ArrayList<T> findAll() {
        ArrayList<T> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getOrderBy()), null);
        if (cursor != null) {
            cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRange(String lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s > ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{lastID});
        if (cursor != null) {
            cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRange(int lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s > ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{String.valueOf(lastID)});
        if (cursor != null) {
            cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRangeInvert(int lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s < ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{String.valueOf(lastID)});
        if (cursor != null) {
            cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public String toFCUpperCase(String e) {
        String finalString = "";
        e = e.trim();
        for (String str : e.split(" ")) {
            finalString = finalString.trim();
            //finalString.concat(finalString.substring(0, 1).toUpperCase()
        }
        return e;
    }

    /**
     * Convert a model to sql query
     * <br> <b>example</b><br>
     * <code>
     * ContentValues query = new ContentValues();<br>
     * if (obj.getId() != 0) {
     * query.put("id", obj.getId());
     * }<br>
     * query.put("types_id", obj.getTypeID());<br>
     * query.put("name", obj.getName());<br>
     * return query;<br>
     * </code>
     *
     * @param obj   the model to convert
     * @param query will contain the value of the of the model in query string
     * @return and object that will be use for update and insert query
     */
    public abstract ContentValues sqlQueryBuilder(T obj, ContentValues query);

    /**
     * Convert a row result to a model
     * <p> <b>example</b><br>
     * <code>
     * return new Model(cursor.getInt(0), cursor.getInt(1), cursor.getString(2));
     * </code>
     *
     * @param cursor object containing all query rows
     * @return the model with all attribute like save in the table
     */
    public abstract T cursorToModel(Cursor cursor);

}
