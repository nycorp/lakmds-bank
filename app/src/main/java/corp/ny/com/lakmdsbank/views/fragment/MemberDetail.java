package corp.ny.com.lakmdsbank.views.fragment;

import android.content.DialogInterface;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import br.com.simplepass.loading_button_lib.interfaces.OnAnimationEndListener;
import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.ParticipationTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.models.Participation;
import corp.ny.com.lakmdsbank.utils.EntryManager;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.dialog.AlertDialogModelSelector;

import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.TOP;
import static corp.ny.com.lakmdsbank.utils.EntryManager.displayError;
import static corp.ny.com.lakmdsbank.utils.EntryManager.hideError;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 26/02/18.
 */

public class MemberDetail extends DialogFragment implements View.OnClickListener, TextWatcher, DialogInterface.OnClickListener {

    public final static String DEFAULT_CONTEXT = "DEFAULT";
    public final static String UPDATE_CONTEXT = "UPDATE";
    private final static String TAG = "TAG";
    private ImageView btnBack;
    private ImageView imageAvatar;
    private TextInputEditText tvMemberName;
    private TextView memberNameError;
    private EditText tvMemberPhone;
    private TextView memberPhoneError;
    private EditText tvMemberEmail;
    private TextView memberEmailError;
    private TextInputEditText txtMemberAccount;
    private TextView memberAccountError;
    private CircularProgressButton btnCreateMember;
    private List<Account> accounts = new ArrayList<>();
    private Member member;
    private String tag = DEFAULT_CONTEXT;
    private AlertDialogModelSelector<Account> dialogAccountSelector;

    public static DialogFragment getInstance() {
        MemberDetail f = new MemberDetail();
        defaultAnimation(f);
        return f;
    }

    public static DialogFragment newInstance(Bundle bundle, String tag) {
        MemberDetail f = new MemberDetail();
        bundle.putString(TAG, tag);
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static DialogFragment defaultAnimation(DialogFragment fragment) {
        fragment.setEnterTransition(new Slide(BOTTOM).setDuration(250));
        fragment.setExitTransition(new Slide(TOP).setDuration(250));
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.fragment_member_details, container, false);
        try {
            //remove layout background color
            InsetDrawable background = (InsetDrawable) getDialog().getWindow().getDecorView().getBackground();
            background.setAlpha(230);
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FIRST_APPLICATION_WINDOW);
        } catch (Exception ex) {
            Log.i("Transparent bg error", ex.getMessage());
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setGravity(Gravity.CENTER);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragmentDialog);
        initialize(view);
    }

    private void initialize(View view) {
        //Sequent.origin((ViewGroup) view).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        findViews(view);
        if (getArguments() != null) {
            if (getArguments().containsKey(Member.class.getName())) {
                member = (Member) getArguments().get(Member.class.getName());
                tvMemberName.setText(member != null ? member.getName() : null);
                tvMemberPhone.setText(member != null ? member.getPhone() : null);
                tvMemberEmail.setText(member != null ? member.getEmail() : null);
                tag = getArguments().getString(TAG);
                assert tag != null;
                if (tag.equals(UPDATE_CONTEXT)) {
                    txtMemberAccount.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        resetAllError();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        activateSubmitButton();
    }

    /**
     * Use this function to hide all error TextView
     */
    public void resetAllError() {
        hideError(tvMemberEmail, memberEmailError);
        hideError(tvMemberName, memberNameError);
        hideError(tvMemberPhone, memberPhoneError);
    }

    /**
     * Enable submit button
     */
    public void activateSubmitButton() {
        if (tvMemberName.getText().length() > 3) {
            if (tvMemberPhone.getText().length() > 3) {
                if (!tag.equals(DEFAULT_CONTEXT)) {
                    btnCreateMember.setEnabled(true);
                    btnCreateMember.setBackground(getContext().getDrawable(R.drawable.active_dot));
                    return;
                }
                if (!accounts.isEmpty()) {
                    btnCreateMember.setEnabled(true);
                    btnCreateMember.setBackground(getContext().getDrawable(R.drawable.active_dot));
                    return;
                }
            }
        }
        btnCreateMember.revertAnimation();
        btnCreateMember.setEnabled(false);
        btnCreateMember.setBackground(getContext().getDrawable(R.drawable.inactive_dot));
    }

    public boolean getError() {
        if (!EntryManager.phoneValidator(tvMemberPhone.getText().toString())) {
            memberPhoneError.setText("Numéro invalide");
            EntryManager.displayError(tvMemberPhone, memberPhoneError);
            return true;
        }
        if (!TextUtils.isEmpty(tvMemberEmail.getText().toString())) {
            if (!EntryManager.emailValidator(tvMemberEmail.getText().toString())) {
                memberEmailError.setText(R.string.error_invalid_email);
                EntryManager.displayError(tvMemberEmail, memberEmailError);
                return true;
            }
        }

        return false;
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        btnBack = container.findViewById(R.id.btn_back);
        imageAvatar = container.findViewById(R.id.image_avatar);
        tvMemberName = container.findViewById(R.id.tv_member_name);
        memberNameError = container.findViewById(R.id.member_name_error);
        tvMemberPhone = container.findViewById(R.id.tv_member_phone);
        memberPhoneError = container.findViewById(R.id.member_phone_error);
        tvMemberEmail = container.findViewById(R.id.tv_member_email);
        memberEmailError = container.findViewById(R.id.member_email_error);
        txtMemberAccount = container.findViewById(R.id.tv_member_account);
        memberAccountError = container.findViewById(R.id.member_account_error);
        btnCreateMember = container.findViewById(R.id.btn_create_member);

        btnCreateMember.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        tvMemberPhone.addTextChangedListener(this);
        tvMemberName.addTextChangedListener(this);
        tvMemberEmail.addTextChangedListener(this);
        txtMemberAccount.setShowSoftInputOnFocus(false);
        txtMemberAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) selectAccount();
            }
        });
        txtMemberAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAccount();
            }
        });
    }

    private void selectAccount() {
        dialogAccountSelector = new AlertDialogModelSelector<>(getContext(),
                new AccountTable().findAll(), "name");
        dialogAccountSelector.setSelectedModels(accounts);
        dialogAccountSelector.setOnClickListener(this);
        try {
            dialogAccountSelector.show();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnCreateMember) {
            // Handle clicks for btnCreateMember
            switch (tag) {
                case UPDATE_CONTEXT:
                    btnCreateMember.startAnimation();
                    btnCreateMember.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (getError()) {
                                btnCreateMember.revertAnimation();
                            } else {
                                member.setEmail(tvMemberEmail.getText().toString());
                                member.setName(tvMemberName.getText().toString());
                                member.setPhone(tvMemberPhone.getText().toString());
                                new MemberTable().update(member);
                                if (member != null) {
                                    btnCreateMember.setDoneColor(getContext().getColor(R.color.colorPrimary));
                                    btnCreateMember.setFinalCornerRadius(1000);
                                    btnCreateMember.revertAnimation(new OnAnimationEndListener() {
                                        @Override
                                        public void onAnimationEnd() {
                                            displayProfile(member);
                                        }
                                    });
                                }
                            }
                        }
                    }, 1500);
                    break;
                default:
                    btnCreateMember.startAnimation();
                    btnCreateMember.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (getError()) {
                                btnCreateMember.revertAnimation();
                            } else {
                                member = Member.getInstance();
                                member.setEmail(tvMemberEmail.getText().toString());
                                member.setName(tvMemberName.getText().toString());
                                member.setPhone(tvMemberPhone.getText().toString());
                                member = new MemberTable().create(member);
                                if (member != null) {
                                    for (Account account : accounts) {
                                        Participation participation = new Participation(member.getId(), account.getId(), Calendar.getInstance().getTime().toLocaleString());
                                        new ParticipationTable().create(participation);
                                    }

                                    btnCreateMember.setDoneColor(getContext().getColor(R.color.colorPrimary));
                                    //btnCreateMember.revertAnimation();
                                    btnCreateMember.stopAnimation();
                                    btnCreateMember.revertAnimation(new OnAnimationEndListener() {
                                        @Override
                                        public void onAnimationEnd() {
                                            displayProfile(member);
                                        }
                                    });

                                }

                            }
                        }
                    }, 1500);
            }

        } else if (v == btnBack) {
            dismiss();
        }
    }

    private void displayProfile(Member member) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Member.class.getName(), member);
        ((Main) getActivity()).displaySelectedScreen(MemberInfo.newInstance(bundle));
        dismiss();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        txtMemberAccount.setText(dialogAccountSelector.getSelectedItemsString());
        accounts = dialogAccountSelector.getSelectedModels();
        if (accounts.isEmpty()) {
            memberAccountError.setText(R.string.choose_an_account);
            displayError(txtMemberAccount, memberAccountError);
        } else {
            hideError(txtMemberAccount, memberAccountError);
        }
        activateSubmitButton();
    }
}
