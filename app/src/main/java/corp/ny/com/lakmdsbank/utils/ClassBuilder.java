package corp.ny.com.lakmdsbank.utils;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class ClassBuilder<T> {

    /**
     * help to find field value by his name in a model
     *
     * @param object    the class model
     * @param fieldName the field name of the desired value
     * @return String value or null if not found
     */
    @Nullable
    public static String getString(@NotNull Object object, String fieldName) throws ClassNotFoundException {
        Class c = Class.forName(object.getClass().getName());
        for (Field field : c.getDeclaredFields()) {
            //skip if it is not the target field
            if (!field.getName().equals(fieldName)) continue;
            //make field accessible for reading
            if (field.getModifiers() == Modifier.PRIVATE) {
                field.setAccessible(true);
            }

            try {
                if (field.getType() == String.class)
                    return String.valueOf(field.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * help to find field value by his name in a model
     *
     * @param object    the class model
     * @param fieldName the field name of the desired value
     * @return int value or -1 if not found
     */
    @Nullable
    public static int getInt(@NotNull Object object, String fieldName) throws ClassNotFoundException {
        Class c = Class.forName(object.getClass().getName());
        for (Field field : c.getDeclaredFields()) {
            //skip if it is not the target field
            if (!field.getName().equals(fieldName)) continue;
            //make field accessible for reading
            if (field.getModifiers() == Modifier.PRIVATE) {
                field.setAccessible(true);
            }

            try {
                if (field.getType() == int.class)
                    return Integer.parseInt(String.valueOf(field.get(object)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    /**
     * @param object
     */
    public void printClass(T object) {
        try {
            Class c = Class.forName(object.getClass().getName());
            System.out.println("{");

            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                try {

                    System.out.println(String.format("\t\"%s\" : \"%s\",", field.getName(), field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            for (Field field : c.getSuperclass().getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                try {
                    System.out.println(String.format("\t\"%s\" : \"%s\",", field.getName(), field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("}");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param json
     * @param object
     * @return
     */
    public T buildClass(final ArrayList json, final T object) {
        int fieldIndex = 0;
        try {
            Class c = Class.forName(object.getClass().getName());

            for (Field field : c.getSuperclass().getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                field.set(object, json.get(fieldIndex));
                fieldIndex++;
            }
            fieldIndex = 0;
            for (Field field : c.getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                field.set(object, json.get(fieldIndex));
                fieldIndex++;
            }

        } catch (ClassNotFoundException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return object;
    }
}
