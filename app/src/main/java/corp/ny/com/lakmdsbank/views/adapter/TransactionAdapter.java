package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class TransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_DEFAULT = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private OnItemClickListener<Operation> itemSelectedListener;
    private Context context;
    private List<Operation> operations = new ArrayList<>();
    private int lastId = -1;

    public TransactionAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return (operations.isEmpty() ? VIEW_TYPE_DEFAULT : VIEW_TYPE_ITEM);
    }

    public boolean addItem(List<Operation> operation) {
        if (operation.isEmpty()) return false;
        if (operations.isEmpty()) lastId = operation.get(0).getId();
        Log.e("Last ID", "" + lastId);
        for (Operation op : operation) {
            if (op.getId() < lastId) lastId = op.getId();
            operations.add(op);
        }
        notifyDataSetChanged();
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                return new SavingHistoryHolder(LayoutInflater.from(context).inflate(R.layout.component_recent_saving_transactions, parent, false));

            default:
                return NoItemFoundViewHolder.build(context, parent);
        }
    }

    public int getLastId() {
        return lastId;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SavingHistoryHolder) {
            final SavingHistoryHolder sectionHolder = (SavingHistoryHolder) holder;
            final Operation operation = operations.get(position);
            Sequent.origin((ViewGroup) sectionHolder.itemView).delay(50).duration(100).offset(10).anim(context, Animation.FADE_IN_LEFT).start();
            sectionHolder.savingName.setText(new AccountTable().find(operation.getSavingID()).getName());
            int type = new OperationTypeTable().find(operation.getTypeID()).getValue();
            sectionHolder.transactionAmount.setText(Amount.toMoney(operation.getAmount() * type));
            if (type < 0) {
                sectionHolder.transactionImage.setImageResource(R.drawable.ic_piggy_retirement);
                sectionHolder.transactionAmount.setTextColor(context.getColor(R.color.accent));
            } else {
                sectionHolder.transactionAmount.setTextColor(context.getColor(R.color.primary_text_dark));
            }
            sectionHolder.transactionInfo.setText(new MemberTable().find(operation.getMemberID()).getName());
            if (position == getItemCount() - 1) {
                //hide divider for the last item
                sectionHolder.divider.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sectionHolder.divider.setVisibility(View.GONE);
                    }
                }, 200);
            }
            sectionHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(operation);
                }
            });
        } else if (holder instanceof NoItemFoundViewHolder) {
            NoItemFoundViewHolder noItemFoundViewHolder = (NoItemFoundViewHolder) holder;
        }

    }

    @Override
    public int getItemCount() {
        return (operations.isEmpty() ? 1 : operations.size());
    }

    public void setItemSelectedListener(OnItemClickListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    private class SavingHistoryHolder extends RecyclerView.ViewHolder {
        private ImageView transactionImage;
        private TextView savingName;
        private TextView transactionInfo;
        private TextView transactionAmount;
        private ImageView divider;

        public SavingHistoryHolder(final View itemView) {
            super(itemView);
            findViews(itemView);
        }

        /**
         * Find the Views in the layout<br />
         */
        private void findViews(View container) {
            transactionImage = container.findViewById(R.id.transaction_image);
            savingName = container.findViewById(R.id.saving_name);
            transactionInfo = container.findViewById(R.id.transaction_info);
            transactionAmount = container.findViewById(R.id.transaction_amount);
            divider = container.findViewById(R.id.divider);
        }
    }
}
