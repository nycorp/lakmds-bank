package corp.ny.com.lakmdsbank.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTable;
import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.preference.Env;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.SectionAdapter;
import corp.ny.com.lakmdsbank.views.adapter.TransactionAdapter;
import corp.ny.com.lakmdsbank.views.listener.EndlessRecyclerViewScrollListener;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 11/02/18.
 */

public class Home extends Fragment implements OnItemClickListener, View.OnClickListener {
    private TextView totalAmount;
    private ImageView amountBg;
    private TextView lastTransactionAmount;
    private FloatingActionButton btnAction;
    private RecyclerView sectionRecycleView;
    private RecyclerView savingRecycleView;
    // Store a member variable for the listener
    private EndlessRecyclerViewScrollListener scrollListener;

    private Main parent;

    public static Home getInstance() {
        // Defines enter transition for all fragment views;
        Home f = new Home();
        f.setEnterTransition(new Slide(BOTTOM).setDuration(400));
        f.setExitTransition(new Slide(START).setDuration(400));
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View rootView) {
        findViews(rootView);

        //toolbar.findViewById(R.id.title).setVisibility(View.GONE);
        sectionRecycleView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        final SectionAdapter sectionAdapter = new SectionAdapter(getContext());
        sectionAdapter.setItemSelectedListener(this);
        sectionRecycleView.setAdapter(sectionAdapter);
        sectionRecycleView.post(new Runnable() {
            @Override
            public void run() {
                for (Account ac : AccountTable.getInstance().findAll()) {
                    sectionAdapter.addItem(ac);
                }
            }
        });

        sectionRecycleView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }
        });

        savingRecycleView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        final TransactionAdapter transactionAdapter = new TransactionAdapter(getContext());
        transactionAdapter.setItemSelectedListener(this);
        savingRecycleView.setAdapter(transactionAdapter);
        savingRecycleView.post(new Runnable() {
            @Override
            public void run() {
                Table<Operation> table = new OperationTable();
                transactionAdapter.addItem(table.findByRange(0));
                scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) savingRecycleView.getLayoutManager()) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        // Triggered only when new data needs to be appended to the list
                        // Add whatever code is needed to append new items to the bottom of the list
                        updateSavingHistory();
                    }
                };

                savingRecycleView.addOnScrollListener(scrollListener);
            }
        });

        totalAmount.post(new Runnable() {
            @Override
            public void run() {
                double amount = new AccountTable().getBankAmount();
                if (amount < 0) {
                    totalAmount.setTextColor(getContext().getColor(R.color.accent));
                }
                totalAmount.setText(Amount.toMoney(amount));
            }
        });

        parent = ((Main) getActivity());

    }

    private void updateSavingHistory() {
        savingRecycleView.post(new Runnable() {
            @Override
            public void run() {
                ((TransactionAdapter) savingRecycleView.getAdapter())
                        .addItem(OperationTable.getInstance()
                                .findByRangeInvert(((TransactionAdapter) savingRecycleView.getAdapter())
                                        .getLastId()));

            }
        });
    }

    @Override
    public void click(Object model) {
        if (model instanceof Account) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Account.class.getName(), (Account) model);
            bundle.putString(Env.BundleDataName.AMOUNT.name(),
                    ((SectionAdapter) sectionRecycleView.getAdapter()).getAccountAmount((Account) model));
            parent.addToHistory(SavingHistory.newInstance(bundle));
        } else if (model instanceof Operation) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(TransactionDetail.class.getName(), (Operation) model);
            parent.addToHistory(TransactionDetail.newInstance(bundle));
        }
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        Sequent.origin((ViewGroup) container).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        totalAmount = container.findViewById(R.id.total_amount);
        amountBg = container.findViewById(R.id.amount_bg);
        lastTransactionAmount = container.findViewById(R.id.last_transaction_amount);
        btnAction = container.findViewById(R.id.btn_action);
        sectionRecycleView = container.findViewById(R.id.section_recycle_view);
        savingRecycleView = container.findViewById(R.id.saving_recycle_view);

        btnAction.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnAction) {
            animateButtonsIn(v);
            // Handle clicks for btnAction
            parent.addToHistory(Transaction.getInstance());
        }
    }

    private void animateButtonsIn(View view) {
        ViewPropertyAnimator viewPropertyAnimator = view.animate()
                .setStartDelay(100)
                //.setInterpolator(new Interpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
    }


}
