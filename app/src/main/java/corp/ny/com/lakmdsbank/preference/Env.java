package corp.ny.com.lakmdsbank.preference;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class Env {
    public static int MAX_UNLOCK_TEMPTATION = 3;
    public static int SYSTEM_LOCK_DURATION = 60 * 5;//Duration in second
    private static int DURATION_BEFORE_LOCK_WHEN_NO_ACTIVITY = 60 * 5;

    public enum BundleDataName {
        AMOUNT
    }
}
