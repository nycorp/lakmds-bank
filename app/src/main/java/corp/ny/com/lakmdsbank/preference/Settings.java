package corp.ny.com.lakmdsbank.preference;

import android.content.SharedPreferences;

import corp.ny.com.lakmdsbank.models.Admin;
import corp.ny.com.lakmdsbank.system.App;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class Settings {

    private static String preferenceName = Settings.class.getName();
    private SharedPreferences preferences;

    private Settings() {
        //Get the shared preferences
        preferences = App.getContext().getSharedPreferences(preferenceName, MODE_PRIVATE);
        testValue();
    }

    public static Settings getInstance() {
        return new Settings();
    }

    /**
     * @param admin
     */
    public void createOrUpdateAdminData(Admin admin) {
        preferences.edit()
                .putString(AdminFields.NAME.toString(), admin.getName())
                .putString(AdminFields.PASSWORD.toString(), admin.getPassword())
                .putString(AdminFields.IMAGE.toString(), admin.getImage())
                .putString(AdminFields.BIRTHDAY.toString(), admin.getBirthDay())
                .apply();
    }

    /**
     * Retrieve admin data
     *
     * @return
     */
    public Admin getAdmin() {
        return Admin.getInstance()
                .setName(preferences.getString(AdminFields.NAME.toString(), null))
                .setPassword(preferences.getString(AdminFields.PASSWORD.toString(), null))
                .setImage(preferences.getString(AdminFields.IMAGE.toString(), null))
                .setBirthDay(preferences.getString(AdminFields.BIRTHDAY.toString(), null));
    }

    private void testValue() {
        createOrUpdateAdminData(Admin.getInstance().
                setName("Queen").setPassword("0513"));
    }

    private enum AdminFields {
        NAME,
        PASSWORD,
        IMAGE,
        BIRTHDAY
    }
}
