package corp.ny.com.lakmdsbank.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.preference.Env;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.SavingHistoryAdapter;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class SavingHistory extends Fragment implements View.OnClickListener, OnItemClickListener {
    private ImageView btnBack;
    private TextView savingName;
    private TextView tvAmount;
    private FloatingActionButton fbAction;
    private RecyclerView savingHistory;
    private Account account;
    private String money;

    public static Fragment getInstance() {
        SavingHistory f = new SavingHistory();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        SavingHistory f = new SavingHistory();
        defaultAnimation(f);
        f.setArguments(bundle);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(400));
        fragment.setExitTransition(new Slide(START).setDuration(400));
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_saving_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
        savingHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        SavingHistoryAdapter historyAdapter = new SavingHistoryAdapter(getContext());
        historyAdapter.setItemSelectedListener(this);
        savingHistory.setAdapter(historyAdapter);
        if (getArguments() != null) {
            if (getArguments().containsKey(Account.class.getName())) {
                account = (Account) getArguments().get(Account.class.getName());
                savingName.setText(account != null ? account.getName() : null);
                for (Operation op : OperationTable.getInstance().getAccountTransaction(account.getId())) {
                    historyAdapter.addItem(op);
                }
            }
            if (getArguments().containsKey(Env.BundleDataName.AMOUNT.name())) {
                money = getArguments().getString(Env.BundleDataName.AMOUNT.name());
                if (AccountTable.getInstance().getAccountAmount(account.getId()) < 0) {
                    tvAmount.setTextColor(getContext().getColor(R.color.accent));
                }
                tvAmount.setText(Amount.toMoney(Double.parseDouble(money)));
            }

        }
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        Sequent.origin((ViewGroup) container).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        btnBack = container.findViewById(R.id.btn_back);
        savingName = container.findViewById(R.id.saving_name);
        tvAmount = container.findViewById(R.id.tv_amount);
        fbAction = container.findViewById(R.id.fb_action);
        savingHistory = container.findViewById(R.id.saving_history);

        fbAction.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == fbAction) {
            // Handle clicks for fbAction
            Bundle bundle = new Bundle();
            bundle.putSerializable(Transaction.class.getName(), account);
            bundle.putString(Env.BundleDataName.AMOUNT.name(), money);
            ((Main) getActivity()).addToHistory(Transaction.newInstance(bundle));
        } else if (v == btnBack) {
            getActivity().onBackPressed();
        }

    }

    @Override
    public void click(Object model) {
        if (model instanceof Operation) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(TransactionDetail.class.getName(), (Operation) model);
            ((Main) getActivity()).addToHistory(TransactionDetail.newInstance(bundle));
        }
    }
}
