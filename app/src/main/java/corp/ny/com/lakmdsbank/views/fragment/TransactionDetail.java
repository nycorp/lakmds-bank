package corp.ny.com.lakmdsbank.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTypeTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.models.OperationType;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.utils.DateManger;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class TransactionDetail extends Fragment {

    private ImageView btnBack;
    private EditText transactionAmount;
    private TextView transactionAmountError;
    private ImageView transactionLogo;
    private ImageView transactionImage;
    private TextView savingName;
    private TextView accountType;
    private TextView bankAmount;
    private ImageView btnChooseAccount;
    private EditText transactionSender;
    private EditText tvTransactionDesc;
    private EditText transactionType;
    private EditText transactionDate;
    private TextView device;
    //model
    private Operation operation;
    private Account account;
    private Member member;

    public static Fragment newInstance(Bundle bundle) {
        TransactionDetail f = new TransactionDetail();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(100));
        fragment.setExitTransition(new Slide(START).setDuration(100));
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transaction_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        Sequent.origin((ViewGroup) view).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        findViews(view);

        if (getArguments() != null) {
            if (getArguments().containsKey(getClass().getName())) {
                operation = (Operation) getArguments().get(getClass().getName());
                assert operation != null;
                account = AccountTable.getInstance().find(operation.getSavingID());
                savingName.setText(account.getName());
                accountType.post(new Runnable() {
                    @Override
                    public void run() {
                        accountType.setText(AccountTypeTable.getInstance().find(account.getTypeID()).getName());
                    }
                });
                bankAmount.setText(Amount.toMoney(AccountTable.getInstance().getAccountAmount(account.getId())));
                transactionAmount.setText(String.valueOf(operation.getAmount()));
                OperationType operationType = OperationTypeTable.getInstance().find(operation.getTypeID());
                if (operationType.getValue() < 0) {
                    transactionLogo.setImageResource(R.drawable.ic_piggy_retirement);
                    device.setText(getString(R.string.negative_africa_devise_symbol, ""));
                }

                transactionSender.setText(MemberTable.getInstance().find(operation.getMemberID()).getName());
                tvTransactionDesc.setText(operation.getDescription());
                transactionType.setText(operationType.getName());


                transactionDate.setText(DateManger.format(operation.getDate()));
            }
        }

    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        Sequent.origin((ViewGroup) container).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        btnBack = container.findViewById(R.id.btn_back);
        transactionAmount = container.findViewById(R.id.transaction_amount);
        transactionAmountError = container.findViewById(R.id.transaction_amount_error);
        transactionLogo = container.findViewById(R.id.transaction_logo);
        transactionImage = container.findViewById(R.id.transaction_image);
        savingName = container.findViewById(R.id.saving_name);
        accountType = container.findViewById(R.id.account_type);
        bankAmount = container.findViewById(R.id.bank_amount);
        btnChooseAccount = container.findViewById(R.id.btn_choose_account);
        transactionSender = container.findViewById(R.id.transaction_sender);
        tvTransactionDesc = container.findViewById(R.id.tv_transaction_desc);
        transactionType = container.findViewById(R.id.transaction_type);
        transactionDate = container.findViewById(R.id.transaction_date);
        device = container.findViewById(R.id.device);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

}
