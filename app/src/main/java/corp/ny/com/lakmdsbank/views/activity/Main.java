package corp.ny.com.lakmdsbank.views.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.utils.ReleaseKeyBoard;
import corp.ny.com.lakmdsbank.views.fragment.About;
import corp.ny.com.lakmdsbank.views.fragment.AccountList;
import corp.ny.com.lakmdsbank.views.fragment.Home;
import corp.ny.com.lakmdsbank.views.fragment.LockScreen;
import corp.ny.com.lakmdsbank.views.fragment.MemberList;
import corp.ny.com.lakmdsbank.views.fragment.SavingHistory;
import corp.ny.com.lakmdsbank.views.fragment.Transaction;
import de.hdodenhof.circleimageview.CircleImageView;

public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Fragment currentScreen;
    private boolean isAppLock = true;
    private List<Fragment> history = new ArrayList<>();
    private CircleImageView avatar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final PopupMenu popupMenu = new PopupMenu(getApplicationContext(),
                getToolBar().findViewById(R.id.expanded_menu));
        popupMenu.getMenuInflater().inflate(R.menu.main, popupMenu.getMenu());
        getToolBar().setOverflowIcon(getDrawable(R.drawable.ic_menu_burger));
        avatar = getToolBar().findViewById(R.id.user_avatar);
        ImageView logo = getToolBar().findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAppLock) return;
                displaySelectedScreen(Home.getInstance());
            }
        });
        //getToolBar().setLogo(getDrawable(R.drawable.logo_name));
        displaySelectedScreen(LockScreen.getInstance());
    }

    public CircleImageView getAvatar() {
        return avatar;
    }

    public Toolbar getToolBar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    @Override
    public void onBackPressed() {
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*if (currentScreen instanceof LockScreen) {
            if (((LockScreen) currentScreen).getfContext() == LockScreen.TRANSACTION_LOCK) {
                return;
            }
        } else*/
        if (!history.isEmpty()) {
            getSupportActionBar().show();
            getWindow().setStatusBarColor(getColor(R.color.primary_light));
            displaySelectedScreen(removeLastScreenFromHistory());
            return;
        }
        super.onBackPressed();

    }

    public void addToHistory(Fragment newScreen) {
        if (isCurrentScreen(newScreen)) return;
        history.add(currentScreen);
        displaySelectedScreen(newScreen);
    }

    public Fragment removeLastScreenFromHistory() {
        return history.remove(history.size() - 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isAppLock) {
            getMenuInflater().inflate(R.menu.lock_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        for (Fragment f : history) {
            if (f instanceof AccountList || f instanceof MemberList) {
                history.remove(f);
                break;
            }
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_manage_accounts) {
            addToHistory(AccountList.getInstance());
        } else if (id == R.id.action_manage_members) {
            addToHistory(MemberList.getInstance());
        } else if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_about) {
            addToHistory(About.getInstance());
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void displaySelectedScreen(Fragment screen) {
        ReleaseKeyBoard keyBoard = new ReleaseKeyBoard(getApplicationContext());
        if (currentScreen != null) {
            keyBoard.hide(currentScreen.getView());
        }
        if (currentScreen instanceof LockScreen && isAppLock) {
            if (!(screen instanceof About)) {
                isAppLock = false;
                onCreateOptionsMenu(getToolBar().getMenu());
            }
        }
        getAvatar().setImageResource(R.drawable.default_avatar);

        //replacing the fragment
        if (screen != null) {
            if (isCurrentScreen(screen)) return;
            currentScreen = screen;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, screen);
            ft.commitNow();
        }
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //drawer.closeDrawer(GravityCompat.START);
    }

    private boolean isCurrentScreen(Fragment screen) {
        if (screen instanceof MemberList && currentScreen instanceof MemberList) {
            return true;
        } else if (screen instanceof Home && currentScreen instanceof Home) {
            return true;
        } else if (screen instanceof SavingHistory && currentScreen instanceof SavingHistory) {
            return true;
        } else if (screen instanceof Transaction && currentScreen instanceof Transaction) {
            return true;
        } else if (screen instanceof AccountList && currentScreen instanceof AccountList) {
            return true;
        } else if (screen instanceof About && currentScreen instanceof About) {
            return true;
        }
        return false;
    }

    public Fragment getCurrentScreen() {
        return currentScreen;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
