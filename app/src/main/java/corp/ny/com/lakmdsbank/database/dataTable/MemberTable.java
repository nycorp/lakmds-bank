package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.models.Operation;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 20/02/18.
 */

public class MemberTable extends Table<Member> {

    public static String buildTable() {
        return "CREATE TABLE IF NOT EXISTS members (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `name` VARCHAR(45) NOT NULL,\n" +
                "  `email` VARCHAR(45) NULL,\n" +
                "  `phone` VARCHAR(45) NOT NULL);";
    }

    public static MemberTable getInstance() {
        return new MemberTable();
    }

    @Override
    public String getTableName() {
        return "members";
    }

    @Override
    public String getOrderBy() {
        return "name";
    }

    @Override
    public int getLimit() {
        return 12;
    }

    @Override
    public String getIdValue(Member obj) {
        return String.valueOf(obj.getId());
    }

    public int countAccounts(int id) {
        int total = 0;
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM participations WHERE %s = %s", "members_id", id), null);
        if (cursor != null) {
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }

    public List<Account> getMyAccounts(int id) {
        ArrayList<Account> result = new ArrayList<>();
        AccountTable table = new AccountTable();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE id IN( SELECT savings_id FROM Participations WHERE members_id = %s)", table.getTableName(), id), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(table.cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public ArrayList<Member> findAllFromAccount(int savingId) {
        ArrayList<Member> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE id IN( SELECT members_id FROM Participations WHERE savings_id = %s) ORDER BY %s", getTableName(), savingId, getOrderBy()), null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * @param id
     * @return
     */
    public double getAllSavings(int id) {
        OperationTable table = new OperationTable();
        double amount = 0f;
        for (Operation op : table.getMemberTransaction(id)) {
            amount += op.getAmount() * table.getOperationType(op.getTypeID()).getValue();
        }
        return amount;
    }

    /**
     * @param memberId
     * @param accountId
     * @return
     */
    public double getSavingBalance(int memberId, int accountId) {
        OperationTable table = new OperationTable();
        double amount = 0f;
        for (Operation op : table.getMemberTransaction(memberId, accountId)) {
            amount += op.getAmount() * table.getOperationType(op.getTypeID()).getValue();
        }
        return amount;
    }

    @Override
    public ContentValues sqlQueryBuilder(Member obj, ContentValues query) {
        if (obj.getId() != 0) {
            query.put("id", obj.getId());
        }
        query.put("name", toFCUpperCase(obj.getName()));
        query.put("email", obj.getEmail().trim());
        query.put("phone", obj.getPhone().trim());
        return query;
    }

    @Override
    public Member cursorToModel(Cursor cursor) {
        return new Member(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
    }

}
