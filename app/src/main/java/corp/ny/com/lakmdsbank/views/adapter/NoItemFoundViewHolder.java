package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import corp.ny.com.lakmdsbank.R;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class NoItemFoundViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView image;
    private TextView text;
    private FloatingActionButton button;
    private View.OnClickListener listener;

    private NoItemFoundViewHolder(View itemView) {
        super(itemView);
        findViews(itemView);
    }

    public static NoItemFoundViewHolder build(Context context, ViewGroup viewGroup) {
        return new NoItemFoundViewHolder(LayoutInflater.from(context).inflate(R.layout.component_no_item, viewGroup, false));
    }

    public FloatingActionButton getButton() {
        return button;
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        image = container.findViewById(R.id.image);
        text = container.findViewById(R.id.text);
        button = container.findViewById(R.id.button);

        button.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == button) {
            if (listener != null)
                listener.onClick(v);
        }
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getText() {
        return text;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }
}
