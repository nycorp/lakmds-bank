package corp.ny.com.lakmdsbank.utils;

import java.text.DecimalFormat;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.system.App;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 22/02/18.
 */

public class Amount {
    public static String toMoney(double amount) {
        String formattedAmount;
        if (amount >= 0) {
            formattedAmount = App.getContext().getString(R.string.africa_devise_symbol, DecimalFormat.getInstance().format(amount));
        } else {
            amount *= -1;
            formattedAmount = App.getContext().getString(R.string.negative_africa_devise_symbol, DecimalFormat.getInstance().format(amount));
        }
        return formattedAmount;
    }

    public static String toMoney(String value) {
        double amount = Double.valueOf(value);
        String formattedAmount;
        if (amount >= 0) {
            formattedAmount = App.getContext().getString(R.string.africa_devise_symbol, DecimalFormat.getInstance().format(amount));
        } else {
            amount *= -1;
            formattedAmount = App.getContext().getString(R.string.negative_africa_devise_symbol, DecimalFormat.getInstance().format(amount));
        }
        return formattedAmount;
    }

}
