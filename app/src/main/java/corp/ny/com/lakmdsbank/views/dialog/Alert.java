package corp.ny.com.lakmdsbank.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import corp.ny.com.lakmdsbank.R;


/**
 * Created by yann-yvan on 17/11/17.
 */

public class Alert {

    private Dialog box;

    //UI references
    private TextView begin;
    private TextView middle;
    private TextView end;
    private Button positiveButton;
    private Button negativeButton;

    //Classes references
    private Context context;

    public Alert(Context context) {
        this.context = context;
        initialize();
    }

    private void initialize() {
        box = new Dialog(context);
        box.requestWindowFeature(Window.FEATURE_NO_TITLE);
        box.setContentView(R.layout.dialog_box);
        box.setCanceledOnTouchOutside(true);

        begin = box.findViewById(R.id.dialog_begin_text);
        middle = box.findViewById(R.id.dialog_middle_text);
        middle.setVisibility(View.GONE);
        end = box.findViewById(R.id.dialog_end_text);
        end.setVisibility(View.GONE);
        positiveButton = box.findViewById(R.id.btn_dialog_ok);
        negativeButton = box.findViewById(R.id.btn_dialog_cancel);
        negativeButton.setVisibility(View.GONE);
    }

    public void setCanceledOnTouchOutside(boolean set) {
        box.setCanceledOnTouchOutside(set);
    }

    public void setBegin(String text) {
        this.begin.setText(text);
    }

    public void setBegin(int res) {
        this.begin.setText(res);
    }

    public void hideBegin() {
        begin.setVisibility(View.GONE);
    }

    public void setMiddle(String text) {
        middle.setVisibility(View.VISIBLE);
        this.middle.setText(text);
    }

    public void setEnd(int res) {
        end.setVisibility(View.VISIBLE);
        this.end.setText(res);
    }

    public void setEnd(String text) {
        end.setVisibility(View.VISIBLE);
        this.end.setText(text);
    }

    public void setPositiveButton(View.OnClickListener onClickListener) {
        this.positiveButton.setOnClickListener(onClickListener);
    }

    public void setPositiveButton(int res, View.OnClickListener onClickListener) {
        this.positiveButton.setOnClickListener(onClickListener);
    }

    public void setNegativeButton(View.OnClickListener onClickListener) {
        negativeButton.setVisibility(View.VISIBLE);
        this.negativeButton.setOnClickListener(onClickListener);
    }

    public void show() {
        box.show();
    }

    public void cancel() {
        box.cancel();
    }
}
