package corp.ny.com.lakmdsbank.models;

import java.io.Serializable;

/**
 * Created by yann-yvan on 20/02/18.
 */

public class Account implements Serializable {
    private int id;
    private int typeID;
    private String name;

    public Account(int id, int typeID, String name) {
        this.id = id;
        this.typeID = typeID;
        this.name = name;
    }

    private Account() {
    }

    public static Account getInsatnce() {
        return new Account();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", typeID=" + typeID +
                ", name='" + name + '\'' +
                '}';
    }
}
