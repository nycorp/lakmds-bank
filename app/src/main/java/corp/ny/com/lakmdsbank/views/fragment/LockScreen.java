package corp.ny.com.lakmdsbank.views.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.transition.Fade;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.models.OperationType;
import corp.ny.com.lakmdsbank.preference.Settings;
import corp.ny.com.lakmdsbank.utils.FingerprintHandler;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.dialog.Alert;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static android.view.Gravity.END;

public class LockScreen extends Fragment {
    public final static int APP_LOCK = 0;
    public final static int TRANSACTION_LOCK = 1;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = LockScreen.class.getName();
    //in case of lock screen before transaction
    Operation operation = null;
    private FrameLayout priceDetail;
    private ImageView btnBack;
    private EditText transactionAmount;
    private LinearLayout defaultStatus;
    private CircleImageView avatar;
    private TextView userName;
    private ImageView passwordDot1;
    private ImageView passwordDot2;
    private ImageView passwordDot3;
    private ImageView passwordDot4;
    private TextView keyPad1;
    private TextView keyPad2;
    private TextView keyPad3;
    private TextView keyPad4;
    private TextView keyPad5;
    private TextView keyPad6;
    private TextView keyPad7;
    private TextView keyPad8;
    private TextView keyPad9;
    private TextView keyPadZero;
    private ImageView keyPadClear;
    private List<ImageView> passwordView;
    private String hintPassword = "";
    private KeyStore keyStore;
    private Cipher cipher;
    private View rootView;
    private TextView device;
    private int fContext = APP_LOCK;

    public static Fragment getInstance() {
        LockScreen f = new LockScreen();
        f.setExitTransition(new Fade(Fade.IN).setDuration(500));
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        LockScreen f = new LockScreen();
        f.setExitTransition(new Fade(Fade.IN).setDuration(400));
        f.setEnterTransition(new Slide(END).setDuration(400));
        f.setArguments(bundle);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_lock_screen, container, false);
        initialize(rootView);
        return rootView;
    }

    public void initialize(View view) {
        findViews(view);
        ((Main) getActivity()).getAvatar().setImageResource(R.drawable.ic_lock);
        passwordView = preparePasswordView();
        setOnCLickListener();
        userName.setText(Settings.getInstance().getAdmin().getName());

        keyPadClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hintPassword.length() > 0) {
                    passwordView.get(hintPassword.length() - 1).setImageResource(R.drawable.inactive_dot);
                    hintPassword = hintPassword.substring(0, hintPassword.length() - 1);
                }
            }
        });

        if (getArguments() != null) {
            if (getArguments().containsKey(Operation.class.getName())) {
                fContext = TRANSACTION_LOCK;
                operation = (Operation) getArguments().get(Operation.class.getName());
                defaultStatus.setVisibility(View.GONE);
                priceDetail.setVisibility(View.VISIBLE);
                transactionAmount.setText(String.valueOf(operation.getAmount()));
                OperationType operationType = OperationTypeTable.getInstance().find(operation.getTypeID());
                if (operationType.getValue() < 0) {
                    device.setText(getString(R.string.negative_africa_devise_symbol, ""));
                }
            }
        }

        Alert alert = new Alert(getContext());
        alert.hideBegin();
        alert.setMiddle(getString(R.string.sorry));


        // Initializing both Android Keyguard Manager and Fingerprint Manager
        KeyguardManager keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);


        // Check whether the device has a Fingerprint sensor.
        if (!fingerprintManager.isHardwareDetected()) {
            /*
              An error message will be displayed if the device does not contain the fingerprint hardware.
              However if you plan to implement a default authentication method,
              you can redirect the user to a default authentication activity from here.
              Example:
              Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
              startActivity(intent);
             */
            TextView textView = view.findViewById(R.id.errorText);
            //textView.setText(R.string.fingerprint_sensor_absent);
        } else {
            // Checks whether fingerprint permission is set on manifest
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                alert.setEnd("Fingerprint authentication permission not enabled");
                //alert.show();
            } else {
                // Check whether at least one fingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    alert.setEnd(getString(R.string.register_at_least_one_fingerprint));
                    alert.show();
                } else {
                    // Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {
                        alert.setEnd(getString(R.string.lock_screen_not_enable));
                        alert.show();
                    } else {
                        generateKey();

                        if (cipherInit()) {
                            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(getContext());
                            helper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                }
            }
        }
    }

    public int getfContext() {
        return fContext;
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        Sequent.origin((ViewGroup) container).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_UP).start();
        rootView = container;
        priceDetail = container.findViewById(R.id.price_detail);
        btnBack = container.findViewById(R.id.btn_back);
        transactionAmount = container.findViewById(R.id.transaction_amount);
        defaultStatus = container.findViewById(R.id.default_status);
        avatar = container.findViewById(R.id.avatar);
        userName = container.findViewById(R.id.user_name);
        passwordDot1 = container.findViewById(R.id.password_dot_1);
        passwordDot2 = container.findViewById(R.id.password_dot_2);
        passwordDot3 = container.findViewById(R.id.password_dot_3);
        passwordDot4 = container.findViewById(R.id.password_dot_4);
        keyPad1 = container.findViewById(R.id.key_pad_1);
        keyPad2 = container.findViewById(R.id.key_pad_2);
        keyPad3 = container.findViewById(R.id.key_pad_3);
        keyPad4 = container.findViewById(R.id.key_pad_4);
        keyPad5 = container.findViewById(R.id.key_pad_5);
        keyPad6 = container.findViewById(R.id.key_pad_6);
        keyPad7 = container.findViewById(R.id.key_pad_7);
        keyPad8 = container.findViewById(R.id.key_pad_8);
        keyPad9 = container.findViewById(R.id.key_pad_9);
        keyPadZero = container.findViewById(R.id.key_pad_zero);
        keyPadClear = container.findViewById(R.id.key_pad_clear);
        device = container.findViewById(R.id.device);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    /**
     * Add event OnclickListener to all key pad
     */
    private void setOnCLickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hintPassword.length() < passwordView.size()) {
                    hintPassword = hintPassword.concat(((TextView) v).getText().toString());
                    passwordView.get(hintPassword.length() - 1).setImageResource(R.drawable.active_dot);
                }
                if (hintPassword.length() == passwordView.size()) {
                    authenticate();
                }
            }
        };
        keyPad1.setOnClickListener(listener);
        keyPad2.setOnClickListener(listener);
        keyPad3.setOnClickListener(listener);
        keyPad4.setOnClickListener(listener);
        keyPad5.setOnClickListener(listener);
        keyPad6.setOnClickListener(listener);
        keyPad7.setOnClickListener(listener);
        keyPad8.setOnClickListener(listener);
        keyPad9.setOnClickListener(listener);
        keyPadZero.setOnClickListener(listener);
    }

    public void simulate() {
        while (hintPassword.length() < passwordView.size())
            hintPassword = hintPassword.concat("0");
        passwordView.get(hintPassword.length() - 1).postDelayed(new Runnable() {
            @Override
            public void run() {
                passwordView.get(hintPassword.length() - 1).setImageResource(R.drawable.active_dot);
            }
        }, 100);
    }

    /**
     * Use this to check if the password is wrong or not
     */
    public void authenticate() {
        if (hintPassword.equals(Settings.getInstance().getAdmin().getPassword())) {
            switch (fContext) {
                case APP_LOCK:
                    ((Main) getActivity()).displaySelectedScreen(Home.getInstance());
                    break;
                case TRANSACTION_LOCK:
                    makeTransaction();
                    break;
            }

        } else {
            final View view = rootView.findViewById(R.id.password_dot_container);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //launch error event
                    YoYo.with(Techniques.Shake)
                            .duration(500)
                            .repeat(0)
                            .playOn(view);
                    //reset the password
                    hintPassword = "";
                    //clean password dot
                    resetInput();
                }
            }, 200);
        }
    }

    public void resetInput() {
        for (ImageView image : passwordView) {
            image.setImageResource(R.drawable.inactive_dot);
        }
    }

    public void makeTransaction() {
        OperationTable.getInstance().create(operation);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Operation.class.getName(), operation);
        ((Main) getActivity()).displaySelectedScreen(TransactionSucceed.newInstance(bundle));
    }

    /**
     * Create a list for password hint displaying containing imageViews
     *
     * @return a list containing all image view
     */
    private List<ImageView> preparePasswordView() {
        List<ImageView> passwordView = new ArrayList<>();
        passwordView.add(passwordDot1);
        passwordView.add(passwordDot2);
        passwordView.add(passwordDot3);
        passwordView.add(passwordDot4);
        return passwordView;
    }

    /**
     *
     */
    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }
}
