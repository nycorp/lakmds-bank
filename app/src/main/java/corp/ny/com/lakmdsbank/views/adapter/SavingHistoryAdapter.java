package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTypeTable;
import corp.ny.com.lakmdsbank.models.Operation;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.utils.DateManger;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class SavingHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_DEFAULT = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private List<Operation> operations = new ArrayList<>();
    private OnItemClickListener<Operation> itemSelectedListener;
    private Context context;

    public SavingHistoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return (operations.isEmpty() ? VIEW_TYPE_DEFAULT : VIEW_TYPE_ITEM);
    }

    public boolean addItem(Operation operation) {
        if (operations.add(operation)) {
            notifyItemInserted(operations.size() - 1);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.component_transaction_history_item, parent, false);
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                return new SavingHistoryHolder(view);

            default:
                return NoItemFoundViewHolder.build(context, parent);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SavingHistoryHolder) {
            final SavingHistoryHolder sectionHolder = (SavingHistoryHolder) holder;
            final Operation op = operations.get(position);
            // Sequent.origin((ViewGroup) sectionHolder.itemView).delay(50).duration(100).offset(10).anim(context, Animation.FADE_IN_LEFT).start();
            sectionHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(op);
                }
            });
            sectionHolder.transactionAmount.post(new Runnable() {
                @Override
                public void run() {
                    int type = new OperationTypeTable().find(op.getTypeID()).getValue();
                    //Log.e("type",""+type);
                    if (type < 0) {
                        sectionHolder.transactionAmount.setTextColor(context.getColor(R.color.accent));
                    } else {
                        sectionHolder.transactionAmount.setTextColor(context.getColor(R.color.primary_text_dark));
                    }
                    sectionHolder.transactionAmount.setText(Amount.toMoney(op.getAmount() * type));
                }
            });

            sectionHolder.memberName.setText(MemberTable.getInstance().find(op.getMemberID()).getName());

            if (DateManger.isRecent(op.getDate())) {
                sectionHolder.statusImage.setImageResource(R.drawable.active_dot);
            } else {
                sectionHolder.statusImage.setImageResource(R.drawable.inactive_dot);
            }

            sectionHolder.transactionDate.setText(DateManger.format(op.getDate()));

        } else if (holder instanceof NoItemFoundViewHolder) {
            NoItemFoundViewHolder noItemFoundViewHolder = (NoItemFoundViewHolder) holder;
            noItemFoundViewHolder.getButton().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return (operations.isEmpty() ? 1 : operations.size());
    }

    public void setItemSelectedListener(OnItemClickListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    private class SavingHistoryHolder extends RecyclerView.ViewHolder {
        private ImageView statusImage;
        private TextView memberName;
        private TextView transactionDate;
        private TextView transactionAmount;

        SavingHistoryHolder(final View itemView) {
            super(itemView);
            findViews(itemView);
        }

        /**
         * Find the Views in the layout<br />
         */

        private void findViews(View container) {
            statusImage = container.findViewById(R.id.status_image);
            memberName = container.findViewById(R.id.member_name);
            transactionDate = container.findViewById(R.id.transaction_date);
            transactionAmount = container.findViewById(R.id.transaction_amount);
        }

    }
}
