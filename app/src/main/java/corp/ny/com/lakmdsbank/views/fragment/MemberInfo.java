package corp.ny.com.lakmdsbank.views.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.database.dataTable.OperationTable;
import corp.ny.com.lakmdsbank.database.dataTable.ParticipationTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.models.Participation;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.MemberBalanceAdapter;
import corp.ny.com.lakmdsbank.views.dialog.AlertDialogModelSelector;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 26/02/18.
 */

public class MemberInfo extends Fragment implements View.OnClickListener, DialogInterface.OnClickListener {

    AlertDialogModelSelector<Account> dialogAccountSelector;
    private ImageView btnBack;
    private ImageView btnEdit;
    private TextView bigName;
    private CircleImageView actionCall;
    private CircleImageView memberAvatar;
    private CircleImageView actionSms;
    private TextView saving;
    private TextView totalAccount;
    private TextView totalAccountString;
    private LinearLayout myAccountWidget;
    private LinearLayout myBalanceWidget;
    private EditText tvMemberName;
    private EditText tvMemberPhone;
    private EditText tvMemberEmail;
    private ImageView btnDeleteMember;
    private Member member;
    private List<Account> accountList;
    private List<Account> newAccounts = new ArrayList<>();
    private double balance = 0;

    public static Fragment getInstance() {
        MemberInfo f = new MemberInfo();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        MemberInfo f = new MemberInfo();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(400));
        fragment.setExitTransition(new Slide(START).setDuration(400));
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_member_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getActivity().getWindow().setStatusBarColor(getContext().getColor(R.color.primary));
        ((Main) getActivity()).getSupportActionBar().hide();
        initialize(view);
    }

    private void initialize(View view) {
        Sequent.origin((ViewGroup) view).delay(50).duration(100).offset(10).anim(getContext(), Animation.FADE_IN_RIGHT).start();
        findViews(view);
        if (getArguments() != null) {
            if (getArguments().containsKey(Member.class.getName())) {
                member = (Member) getArguments().get(Member.class.getName());
                bigName.setText(member != null ? member.getName() : null);
                tvMemberName.setText(member != null ? member.getName() : null);
                tvMemberPhone.setText(member != null ? getString(R.string.cmr_code, member.getPhone()) : null);
                tvMemberEmail.setText(member != null ? member.getEmail() : null);
                balance = member != null ? MemberTable.getInstance().getAllSavings(member.getId()) : 0;
                saving.setText(Amount.toMoney(balance));
                int accounts = MemberTable.getInstance().countAccounts(member.getId());
                totalAccount.setText(member != null ? (accounts > 9 ? String.valueOf(accounts) : String.format("0%s", accounts)) : null);
                totalAccountString.setText(getResources().getQuantityString(R.plurals.account_count, accounts, ""));
            }
        }
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        btnBack = container.findViewById(R.id.btn_back);
        btnEdit = container.findViewById(R.id.btn_edit);
        bigName = container.findViewById(R.id.big_name);
        actionCall = container.findViewById(R.id.action_call);
        memberAvatar = container.findViewById(R.id.member_avatar);
        actionSms = container.findViewById(R.id.action_sms);
        saving = container.findViewById(R.id.saving);
        totalAccount = container.findViewById(R.id.total_account);
        totalAccountString = container.findViewById(R.id.total_account_string);
        myAccountWidget = container.findViewById(R.id.my_accounts);
        myBalanceWidget = container.findViewById(R.id.my_balance);
        tvMemberName = container.findViewById(R.id.tv_member_name);
        tvMemberPhone = container.findViewById(R.id.tv_member_phone);
        tvMemberEmail = container.findViewById(R.id.tv_member_email);
        btnDeleteMember = container.findViewById(R.id.btn_delete_member);

        btnBack.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        actionCall.setOnClickListener(this);
        actionSms.setOnClickListener(this);
        myAccountWidget.setOnClickListener(this);
        myBalanceWidget.setOnClickListener(this);
        btnDeleteMember.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            // Handle clicks for btnCreateMember
            getActivity().onBackPressed();
        } else if (v == btnEdit) {
            //prepare bundle to share data
            Bundle bundle = new Bundle();
            bundle.putSerializable(Member.class.getName(), member);
            MemberDetail.newInstance(bundle, MemberDetail.UPDATE_CONTEXT).show(getFragmentManager(), "Edit member");
        } else if (v == actionCall) {
            Intent call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tvMemberPhone.getText().toString()));
            if (call.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(call);
            }
        } else if (v == actionSms) {
            Intent sms = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + tvMemberPhone.getText().toString()));
            startActivity(sms);
        } else if (v == myAccountWidget) {
            newAccounts.clear();
            accountList = MemberTable.getInstance().getMyAccounts(member.getId());
            dialogAccountSelector = new AlertDialogModelSelector<>(getContext(),
                    new AccountTable().findAll(), "name");
            dialogAccountSelector.setSelectedModels(accountList);
            dialogAccountSelector.setOnClickListener(this);
            try {
                dialogAccountSelector.show();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else if (v == btnDeleteMember) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            Drawable icon = getContext().getDrawable(R.drawable.ic_warning_black_24dp);
            icon.setTint(getContext().getColor(R.color.accent));
            alert.setIcon(icon);
            alert.setTitle(R.string.warning);
            alert.setMessage(getString(R.string.delete_member, member.getName()));
            alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            alert.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (MemberTable.getInstance().delete(member)) {
                        getActivity().onBackPressed();
                    }
                }
            });

            alert.create().show();
        } else if (v == myBalanceWidget) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            Drawable icon = getContext().getDrawable(R.drawable.ic_info_black_24dp);
            icon.setTint(getContext().getColor(R.color.primary));
            alert.setIcon(icon);
            alert.setTitle("Solde des comptes");
            View view = View.inflate(getContext(), R.layout.dialog_account_balance, null);
            TextView totalAmount = view.findViewById(R.id.total_amount);
            if (balance < 0) {
                totalAmount.setText(Amount.toMoney(balance));
                totalAmount.setTextColor(getContext().getColor(R.color.accent));
            } else {
                totalAmount.setText(Amount.toMoney(balance));
            }

            RecyclerView recyclerView = view.findViewById(R.id.rv_account_balances);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            MemberBalanceAdapter balanceAdapter = new MemberBalanceAdapter(getContext(), member);
            balanceAdapter.addItem(MemberTable.getInstance().getMyAccounts(member.getId()));
            recyclerView.setAdapter(balanceAdapter);
            alert.setView(view);

            alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alert.create().show();
        }
    }

    private void sortAccount(List<Account> newAccounts) {
        Log.e("Size", String.valueOf(newAccounts.size()));
        for (Account account : newAccounts) {
            if (accountList.contains(account)) {
                accountList.remove(account);
            } else {
                this.newAccounts.add(account);
            }
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        Drawable icon = getContext().getDrawable(R.drawable.ic_warning_black_24dp);
        icon.setTint(getContext().getColor(R.color.accent));
        alert.setIcon(icon);
        alert.setCancelable(false);
        alert.setTitle(R.string.warning);
        sortAccount(dialogAccountSelector.getSelectedModels());
        if (accountList.isEmpty() && newAccounts.isEmpty()) return;
        String oldAccount = "";
        if (!accountList.isEmpty()) {
            oldAccount = String.format("<p>%s</p><table>", "Toutes vos transactions relative a ces comptes seront supprimer de manière definitive");
            for (Account account : accountList) {
                oldAccount = oldAccount.concat(String.format("<td>%s</td><br/>", account.getName()));
            }
            oldAccount = oldAccount.concat("</table>");
        }

        if (!newAccounts.isEmpty()) {
            oldAccount += String.format("<p>%s</p><table>", "Bienvenue dans vos nouveaux comptes");
            for (Account account : newAccounts) {
                oldAccount = oldAccount.concat(String.format("<td>%s</td><br/>", account.getName()));
            }
            oldAccount = oldAccount.concat("</table>");
        }

        alert.setMessage(Html.fromHtml(oldAccount));
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ParticipationTable table = ParticipationTable.getInstance();
                Participation participation;
                for (Account account : accountList) {
                    participation = new Participation(member.getId(), account.getId(), "");
                    table.delete(participation);
                    OperationTable.getInstance().delete(member.getId(), account.getId());
                }

                for (Account account : newAccounts) {
                    participation = new Participation(member.getId(), account.getId(), "");
                    table.create(participation);
                }
                balance = member != null ? MemberTable.getInstance().getAllSavings(member.getId()) : 0;
                saving.setText(Amount.toMoney(balance));
                int accounts = MemberTable.getInstance().countAccounts(member.getId());
                totalAccount.setText(member != null ? (accounts > 9 ? String.valueOf(accounts) : String.format("0%s", accounts)) : null);
            }
        });

        alert.create().show();
    }
}
