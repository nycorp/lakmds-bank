package corp.ny.com.lakmdsbank.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 11/04/18.
 */

public class Pack<N, Y> {
    private List<N> keys = new ArrayList<>();
    private List<Y> values = new ArrayList<>();


    public void put(N key, Y value) {
        keys.add(key);
        values.add(value);
    }


    public Y get(N key) {
        return values.get(keys.indexOf(key));
    }

    public List<N> keySet() {
        return keys;
    }

    public List<Y> values() {
        return values;
    }

    public void updateValue(N key, Y value) {
        values.remove(keys.indexOf(key));
        values.add(keys.indexOf(key), value);
    }

    public void clean() {
        keys.clear();
        values.clear();
    }

}
