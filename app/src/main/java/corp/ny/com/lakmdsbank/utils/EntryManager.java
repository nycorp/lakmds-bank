package corp.ny.com.lakmdsbank.utils;

import android.view.View;


/**
 * Created by yann-yvan on 15/01/18.
 */

public class EntryManager {
    /**
     * @param email that you want to check structure
     *              sample : foo@example.com
     * @return TRUE if email is well formed or FALSE if not
     */
    public static boolean emailValidator(String email) {
        String model = "([\\w-[.]]{3,}+)@([a-zA-Z]{3,}+)[.]([a-zA-Z]{2,}+)";
        return email.matches(model);
    }

    /**
     * @param phone that you want to check structure
     *              sample : 695499969
     * @return TRUE if email is well formed or FALSE if not
     */
    public static boolean phoneValidator(String phone) {
        String model = "(6[^01][\\d]{7})";
        return phone.matches(model);
    }

    /**
     * @param password check if there not special characters
     *                 such as #&'(){}~"[]^\`|*./§£$µ%+< >;:,=
     * @return TRUE if one of that characters doesn't exist in the sequence and FALSE if there is a special character
     */
    public static boolean passwordValidator(String password) {
        String model = "([\\w@-]{6,20}+)";
        return password.matches(model);
    }

    /**
     * Use this function to hide error label
     *
     * @param field represent where user has enter text
     * @param label here will be the error message
     */
    public static void hideError(View field, View label) {
        //field.setBackground(App.getContext().getDrawable(R.drawable.normal_edit_text));
        if (label.getVisibility() == View.VISIBLE) {
            label.setVisibility(View.GONE);
        }
    }

    /**
     * Use this function to display error label
     *
     * @param field represent where user has enter text
     * @param label here will be the error message
     */
    public static void displayError(View field, View label) {
        //Animation animation = AnimationUtils.loadAnimation(App.getContext(), R.anim.car_picture);
        //field.setBackground(App.getContext().getDrawable(R.drawable.edit_text_error));
        if (label.getVisibility() == View.GONE) {
            //label.startAnimation(animation);
            label.setVisibility(View.VISIBLE);
        }
    }
}
