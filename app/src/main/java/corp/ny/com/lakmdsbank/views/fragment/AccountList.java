package corp.ny.com.lakmdsbank.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.preference.Env;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.AccountListAdapter;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by yann-yvan on 17/02/18.
 */

public class AccountList extends Fragment implements View.OnClickListener, OnItemClickListener {
    private ImageView btnBack;
    private TextView tvAmount;
    private RecyclerView accountList;
    private FloatingActionButton buttonAction;
    private Account account;
    private String money;

    public static Fragment getInstance() {
        AccountList f = new AccountList();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        AccountList f = new AccountList();
        defaultAnimation(f);
        f.setArguments(bundle);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        fragment.setEnterTransition(new Slide(END).setDuration(400));
        fragment.setExitTransition(new Slide(START).setDuration(400));
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
        accountList.setLayoutManager(new LinearLayoutManager(getContext()));
        AccountListAdapter adapter = new AccountListAdapter(getContext());
        adapter.addItem(AccountTable.getInstance().findAll());
        adapter.setItemSelectedListener(this);
        accountList.setAdapter(adapter);
    }


    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == buttonAction) {
            // Handle clicks for fbAction
            Bundle bundle = new Bundle();
            bundle.putSerializable(Transaction.class.getName(), account);
            bundle.putString(Env.BundleDataName.AMOUNT.name(), money);
            AccountInfoManagement.getInstance().show(getFragmentManager(), "Create account");
        } else if (v == btnBack) {
            getActivity().onBackPressed();
        }

    }

    @Override
    public void click(Object model) {
        if (model instanceof Account) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Account.class.getName(), (Account) model);
            bundle.putString(Env.BundleDataName.AMOUNT.name(), String.valueOf(
                    (AccountTable.getInstance().getAccountAmount((((Account) model).getId())))));
            ((Main) getActivity()).addToHistory(SavingHistory.newInstance(bundle));
        }
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        btnBack = container.findViewById(R.id.btn_back);
        tvAmount = container.findViewById(R.id.tv_amount);
        accountList = container.findViewById(R.id.account_list);
        buttonAction = container.findViewById(R.id.button_action);

        buttonAction.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }


}
