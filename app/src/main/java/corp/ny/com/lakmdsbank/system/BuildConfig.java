/**
 * Automatically generated file. DO NOT MODIFY
 */
package corp.ny.com.lakmdsbank.system;

public final class BuildConfig {
    public static final boolean DEBUG = true;
    public static final String APPLICATION_ID = "corp.ny.com.lakmdsbank";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 2;
    public static final String VERSION_NAME = "1.01 Beta";
}
