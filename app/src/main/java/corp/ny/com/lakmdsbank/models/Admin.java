package corp.ny.com.lakmdsbank.models;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class Admin {
    private String name;
    private String password;
    private String image;
    private String birthDay;

    private Admin() {
    }

    public static Admin getInstance() {
        return new Admin();
    }

    public String getName() {
        return name;
    }

    public Admin setName(String name) {
        this.name = name;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Admin setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Admin setImage(String image) {
        this.image = image;
        return this;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public Admin setBirthDay(String birthDay) {
        this.birthDay = birthDay;
        return this;
    }
}
