package corp.ny.com.lakmdsbank.models;

/**
 * Created by yann-yvan on 20/02/18.
 */

public class Participation {
    private int memberID;
    private int savingID;
    private String entrance;

    private Participation() {
    }

    public Participation(int memberID, int savingID, String entrance) {
        this.memberID = memberID;
        this.savingID = savingID;
        this.entrance = entrance;
    }

    public static Participation getInsatance() {
        return new Participation();
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public int getSavingID() {
        return savingID;
    }

    public void setSavingID(int savingID) {
        this.savingID = savingID;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    @Override
    public String toString() {
        return "Participation{" +
                ", memberID=" + memberID +
                ", savingID=" + savingID +
                ", entrance='" + entrance + '\'' +
                '}';
    }
}
