package corp.ny.com.lakmdsbank.models;

/**
 * Created by yann-yvan on 20/02/18.
 */

public class OperationType {
    private int id;
    private String name;
    private int value;

    public OperationType(int id, String name, int value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    private OperationType() {
    }

    public static OperationType getInstance() {
        return new OperationType();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OperationType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
