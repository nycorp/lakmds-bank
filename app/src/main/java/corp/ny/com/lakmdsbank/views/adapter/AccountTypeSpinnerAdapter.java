package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.models.AccountType;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 26/02/18.
 */

public class AccountTypeSpinnerAdapter extends SpinnerAdapter<AccountType> {

    public AccountTypeSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, List<AccountType> itemList) {
        super(context, resource, itemList);
        itemList.add(0, new AccountType(0, context.getString(R.string.choose_account_type)));
    }

    @Override
    View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = getInflater().inflate(getResource(), parent, false);
        TextView name = view.findViewById(R.id.tv_name);
        if (position > 0) {
            name.setTextColor(ColorStateList.valueOf(getContext().getColor(R.color.primary_text_dark)));
        }
        name.setText(getItemList().get(position).getName());

        return view;
    }
}
