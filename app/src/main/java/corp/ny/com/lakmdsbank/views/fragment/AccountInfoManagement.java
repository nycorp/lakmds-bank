package corp.ny.com.lakmdsbank.views.fragment;

import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTable;
import corp.ny.com.lakmdsbank.database.dataTable.AccountTypeTable;
import corp.ny.com.lakmdsbank.models.Account;
import corp.ny.com.lakmdsbank.models.AccountType;
import corp.ny.com.lakmdsbank.utils.EntryManager;
import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.adapter.AccountTypeSpinnerAdapter;
import corp.ny.com.lakmdsbank.views.adapter.SpinnerAdapter;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 08/03/18.
 */

public class AccountInfoManagement extends DialogFragment implements View.OnClickListener, OnItemClickListener<AccountType> {
    private LinearLayout contentPanel;
    private TextView dialogTitle;
    private EditText tvAccountName;
    private TextView accountNameError;
    private Spinner spAccountType;
    private TextView accountTypeError;
    private Button btnDialogCancel;
    private Button btnDialogOk;

    //model
    private AccountType accountType;

    public static AccountInfoManagement getInstance() {
        return new AccountInfoManagement();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.modal_account_view, container, false);
        try {
            //remove layout background color
            InsetDrawable background = (InsetDrawable) getDialog().getWindow().getDecorView().getBackground();
            background.setAlpha(0);
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FIRST_APPLICATION_WINDOW);
        } catch (Exception ex) {
            Log.i("Transparent bg error", ex.getMessage());
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setGravity(Gravity.CENTER);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragmentDialog);
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
        spAccountType.post(new Runnable() {
            @Override
            public void run() {
                SpinnerAdapter<AccountType> adapter = new AccountTypeSpinnerAdapter(getContext(), R.layout.component_spinner_view, AccountTypeTable.getInstance().findAll());
                spAccountType.setAdapter(adapter);
                adapter.setItemClickListener(AccountInfoManagement.this);
            }
        });

        btnDialogCancel.setOnClickListener(this);

        btnDialogOk.setOnClickListener(this);
    }


    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        contentPanel = container.findViewById(R.id.contentPanel);
        dialogTitle = container.findViewById(R.id.dialog_title);
        tvAccountName = container.findViewById(R.id.tv_account_name);
        accountNameError = container.findViewById(R.id.account_name_error);
        spAccountType = container.findViewById(R.id.sp_account_type);
        accountTypeError = container.findViewById(R.id.account_type_error);
        btnDialogCancel = container.findViewById(R.id.btn_dialog_cancel);
        btnDialogOk = container.findViewById(R.id.btn_dialog_ok);

        btnDialogCancel.setOnClickListener(this);
        btnDialogOk.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnDialogCancel) {
            // Handle clicks for btnDialogCancel
            getDialog().cancel();
        } else if (v == btnDialogOk) {
            // Handle clicks for btnDialogOk
            if (!getError()) {

                if (AccountTable.getInstance().
                        create(new Account(0, accountType.getId(), tvAccountName.getText().toString())) != null) {
                    ((Main) getActivity()).displaySelectedScreen(AccountList.getInstance());
                    getDialog().dismiss();
                }
            }
        }
    }

    public boolean getError() {
        if (TextUtils.isEmpty(tvAccountName.getText())) {
            accountNameError.setText("Saisir un nom de compte");
            EntryManager.displayError(tvAccountName, accountNameError);
            return true;
        } else if (accountType == null) {
            accountTypeError.setText("Selectionner un type de compte");
            EntryManager.displayError(spAccountType, accountTypeError);
            return true;
        } else if (AccountTable.getInstance().checkIfExist(tvAccountName.getText().toString().trim())) {
            accountNameError.setText("Nom de compte existant");
            EntryManager.displayError(tvAccountName, accountNameError);
            return true;
        }

        return false;
    }

    @Override
    public void click(AccountType model) {
        if (model.getId() == 0) {
            accountTypeError.setText("Selectionner un type de compte");
            EntryManager.displayError(spAccountType, accountTypeError);
            accountType = null;
        } else {
            accountType = model;
        }
    }
}
