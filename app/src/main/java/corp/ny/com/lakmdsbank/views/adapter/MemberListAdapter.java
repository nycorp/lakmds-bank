package corp.ny.com.lakmdsbank.views.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fujiyuu75.sequent.Animation;
import com.fujiyuu75.sequent.Sequent;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.lakmdsbank.R;
import corp.ny.com.lakmdsbank.database.dataTable.MemberTable;
import corp.ny.com.lakmdsbank.models.Member;
import corp.ny.com.lakmdsbank.utils.Amount;
import corp.ny.com.lakmdsbank.views.listener.OnItemClickListener;


/**
 * Created by yann-yvan on 17/02/18.
 */

public class
MemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_DEFAULT = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private OnItemClickListener<Member> itemSelectedListener;
    private Context context;
    private List<Member> members = new ArrayList<>();

    public MemberListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return (members.isEmpty() ? VIEW_TYPE_DEFAULT : VIEW_TYPE_ITEM);
    }

    public boolean addItem(List<Member> members) {
        for (Member member : members) {
            this.members.add(member);
        }
        notifyDataSetChanged();
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                return new MemberHolder(LayoutInflater.from(context).inflate(R.layout.component_member_list, parent, false));

            default:
                return NoItemFoundViewHolder.build(context, parent);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        //Log.e("Position", "" + position);
        if (holder instanceof MemberHolder) {
            final MemberHolder memberHolder = (MemberHolder) holder;
            final Member member = members.get(position);

            Sequent.origin((ViewGroup) memberHolder.itemView).delay(50).duration(100).offset(10).anim(context, Animation.FADE_IN_LEFT).start();
            memberHolder.memberName.setText(member.getName());
            double amount = MemberTable.getInstance().getAllSavings(member.getId());
            if (amount < 0) {
                memberHolder.totalAmount.setTextColor(context.getColor(R.color.accent));
            }
            memberHolder.totalAmount.setText(Amount.toMoney(amount));
            int accounts = MemberTable.getInstance().countAccounts(member.getId());
            memberHolder.totalAccount.setText(
                    context.getResources().getQuantityString(R.plurals.account_count, accounts, accounts)
            );
            memberHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(member);
                }
            });
            memberHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        vibrator.vibrate(200);
                    }
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    Drawable icon = context.getDrawable(R.drawable.ic_warning_black_24dp);
                    icon.setTint(context.getColor(R.color.accent));
                    alert.setIcon(icon);
                    alert.setTitle(R.string.warning);
                    alert.setMessage(context.getString(R.string.delete_member, member.getName()));
                    alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    alert.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (MemberTable.getInstance().delete(member)) {
                                members.remove(member);
                                notifyItemRemoved(holder.getAdapterPosition());
                            }
                        }
                    });

                    alert.create().show();
                    return false;
                }
            });
        } else if (holder instanceof NoItemFoundViewHolder) {
            NoItemFoundViewHolder noItemFoundViewHolder = (NoItemFoundViewHolder) holder;
            noItemFoundViewHolder.getImage().setImageResource(R.drawable.default_avatar);
            noItemFoundViewHolder.getText().setText(R.string.empty_list);
        }

    }

    @Override
    public int getItemCount() {
        return (members.isEmpty() ? 1 : members.size());
    }

    public void setItemSelectedListener(OnItemClickListener<Member> itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    private class MemberHolder extends RecyclerView.ViewHolder {
        private ImageView memberImage;
        private TextView memberName;
        private TextView totalAccount;
        private TextView totalAmount;
        private ImageView divider;

        MemberHolder(final View itemView) {
            super(itemView);
            findViews(itemView);
        }

        /**
         * Find the Views in the layout<br />
         */
        private void findViews(View container) {
            memberImage = container.findViewById(R.id.member_image);
            memberName = container.findViewById(R.id.member_name);
            totalAccount = container.findViewById(R.id.total_account);
            totalAmount = container.findViewById(R.id.total_amount);
            divider = container.findViewById(R.id.divider);
        }

    }
}
