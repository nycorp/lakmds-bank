package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.Participation;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class ParticipationTable extends Table<Participation> {

    public static ParticipationTable getInstance() {
        return new ParticipationTable();
    }

    public static String buildTable() {
        return "CREATE TABLE IF NOT EXISTS participations (\n" +
                "  `members_id` INT NOT NULL,\n" +
                "  `savings_id` INT NOT NULL,\n" +
                "  `entrance_date` TIMESTAMP(12) NOT NULL,\n" +
                "   PRIMARY KEY (`members_id`, `savings_id`),\n" +
                "  CONSTRAINT `fk_members_has_savings_members`\n" +
                "    FOREIGN KEY (`members_id`)\n" +
                "    REFERENCES `members` (`id`)\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE,\n" +
                "  CONSTRAINT `fk_members_has_savings_savings1`\n" +
                "    FOREIGN KEY (`savings_id`)\n" +
                "    REFERENCES `accounts` (`id`)\n" +
                "    ON DELETE CASCADE\n" +
                "    ON UPDATE CASCADE);";
    }

    @Override
    public Participation create(Participation obj) {
        long success = getDb().insert(getTableName(), null, sqlQueryBuilder(obj, new ContentValues()));
        if (success > 0) {
            return obj;
        }
        return null;
    }

    @Override
    public boolean delete(Participation obj) {
        int success = getDb().delete(getTableName(),
                String.format("%s=? AND %s=?", getIdName(), "savings_id"),
                new String[]{getIdValue(obj), String.valueOf(obj.getSavingID())});
        return success > 0;
    }

    @Override
    public String getIdName() {
        return "members_id";
    }

    @Override
    public String getTableName() {
        return "participations";
    }

    @Override
    public String getIdValue(Participation obj) {
        return String.valueOf(obj.getMemberID());
    }

    @Override
    public ContentValues sqlQueryBuilder(Participation obj, ContentValues query) {
        query.put("members_id", obj.getMemberID());
        query.put("savings_id", obj.getSavingID());
        query.put("entrance_date", obj.getEntrance());
        return query;
    }

    @Override
    public Participation cursorToModel(Cursor cursor) {
        return new Participation(cursor.getInt(0), cursor.getInt(1), cursor.getString(2));
    }
}
