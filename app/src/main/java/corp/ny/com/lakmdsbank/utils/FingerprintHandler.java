package corp.ny.com.lakmdsbank.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import corp.ny.com.lakmdsbank.views.activity.Main;
import corp.ny.com.lakmdsbank.views.fragment.Home;
import corp.ny.com.lakmdsbank.views.fragment.LockScreen;

/**
 * Created by whit3hawks on 11/16/16.
 */
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;

    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        //this.update("Fingerprint Authentication error\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        //this.update("Fingerprint Authentication help\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        //this.update(context.getString(R.string.wrong_fingerprint));
        if (((Main) context).getCurrentScreen() instanceof LockScreen) {
            LockScreen lock = ((LockScreen) ((Main) context).getCurrentScreen());
            //lock.simulate();
            lock.authenticate();
        }

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        if (((Main) context).getCurrentScreen() instanceof LockScreen) {
            LockScreen lock = ((LockScreen) ((Main) context).getCurrentScreen());
            switch (lock.getfContext()) {
                case LockScreen.APP_LOCK:
                    ((Main) context).displaySelectedScreen(new Home());
                    break;
                case LockScreen.TRANSACTION_LOCK:
                    lock.makeTransaction();
                    break;
            }
        }
    }

    private void update(String e) {
        Toast.makeText(context, e, Toast.LENGTH_SHORT).show();
    }

}
