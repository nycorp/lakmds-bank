package corp.ny.com.lakmdsbank.database.dataTable;

import android.content.ContentValues;
import android.database.Cursor;

import corp.ny.com.lakmdsbank.database.migrations.Table;
import corp.ny.com.lakmdsbank.models.AccountType;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 21/02/18.
 */

public class AccountTypeTable extends Table<AccountType> {
    public static String buildTable() {
        return "CREATE TABLE IF NOT EXISTS account_types (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `name` VARCHAR(45) NULL);";
    }

    public static String insertDefaultData() {
        return "INSERT INTO `account_types` (id,name) VALUES (1,'Epargne');";
    }

    public static AccountTypeTable getInstance() {
        return new AccountTypeTable();
    }

    @Override
    public String getTableName() {
        return "account_types";
    }

    @Override
    public String getOrderBy() {
        return "name";
    }

    @Override
    public String getIdValue(AccountType obj) {
        return String.valueOf(obj.getId());
    }

    @Override
    public ContentValues sqlQueryBuilder(AccountType obj, ContentValues query) {
        if (obj.getId() != 0) {
            query.put("id", obj.getId());
        }
        query.put("name", obj.getName());
        return query;
    }

    @Override
    public AccountType cursorToModel(Cursor cursor) {
        return new AccountType(cursor.getInt(0), cursor.getString(1));
    }

    enum AccountTypeFields {
        account_types,
        id,
        name
    }
}
